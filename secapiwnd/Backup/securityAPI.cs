using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using secapiwnd.commands;

namespace secapiwnd
{
    /// <summary>
    /// Clase de Seguridad
    /// </summary>
    public class securityAPI : IDisposable
    {
        private bool disposed = false;

        public securityAPI()
        {
            Log.Insertar(Log.DEB, "SECAPIWND: Iniciando DLL");

            cHeader.nomDLL = Util.getVariableEntorno() + @"\Apissechsm.dll";
            //string nomDLL = @"\Apissechsm.dll";

            bool isLibLoaded = Util.CargarLibreria();
            Log.Insertar(Log.DEB, "Resultado de carga de librer�a: " + isLibLoaded.ToString());
            
            Util.InicializarRuta();
        }

        public void Dispose()
        {
            Log.Insertar(Log.DEB, "SECAPIWND: Ejecutando Dispose()");
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            Log.Insertar(Log.DEB, "SECAPIWND: Ejecutando Dispose(" + disposing.ToString() + ")");
            if (!this.disposed)
            {
                if (disposing)
                {
                    /* Dispose managed resources */
                }
                
                /* Dispose unmanaged resources */
                if (!cHeader.hModule.Equals(IntPtr.Zero))
                {
                    cHeader.FreeLibrary(cHeader.hModule);
                    cHeader.hModule = IntPtr.Zero;
                }
                disposed = true;
            }
        }

        ~securityAPI()
        {
            Log.Insertar(Log.DEB, "SECAPIWND: Finalizando DLL");
            Dispose(false);
            Log.Insertar(Log.DEB, "SECAPIWND: DLL finalizada");
        }

        /// <summary>
        /// 
        /// </summary>
        private string dbname = string.Empty;
        private string dbserver = string.Empty;
        private string dbuser = string.Empty;
        private string dbpass = string.Empty;

        private string sBuffer; // Para usarla en las funciones GetSection(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intLongitudClavePrivada"></param>
        /// <param name="intLongitudExponenteClavePublica"></param>
        /// <param name="lngExponenteClavePublica"></param>
        /// <param name="strClavePublicaCifradaSalida"></param>
        /// <param name="strClavePrivadaCifradaSalida"></param>
        /// <returns></returns>
        public int fGenerarClaves_RSA(int intLongitudClavePrivada, int intLongitudExponenteClavePublica,
                                      long lngExponenteClavePublica, ref string strClavePublicaCifradaSalida,
                                      ref string strClavePrivadaCifradaSalida)
        {

            int codigoRetorno = 0;
            
            try
            {

                codigoRetorno = cGenerarClaves_RSA.fGenerarClaves_RSA(intLongitudClavePrivada, intLongitudExponenteClavePublica,
                                                                 lngExponenteClavePublica, ref strClavePublicaCifradaSalida,
                                                                 ref strClavePrivadaCifradaSalida);
            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }
            

            return codigoRetorno;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="intTipoClave"></param>
        /// <param name="intTipoLongitudClave"></param>
        /// <param name="strClaveDESCifradaSalida"></param>
        /// <param name="strValorVerificacionClaveDESCifradaSalida"></param>
        /// <returns></returns>
        public int fGeneraClaveAleatoria_3DES(int intTipoClave, int intTipoLongitudClave,
                                              ref string strClaveDESCifradaSalida,
                                              ref string strValorVerificacionClaveDESCifradaSalida)
        {

            int codigoRetorno = 0;

            try
            {
                codigoRetorno = cGeneraClaveAleatoria_3DES.fGeneraClaveAleatoria_3DES(intTipoClave, intTipoLongitudClave,
                                                           ref strClaveDESCifradaSalida,
                                                           ref strValorVerificacionClaveDESCifradaSalida);

            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }

            
            return codigoRetorno;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strCodigoClaveDES"></param>
        /// <param name="strMensaje"></param>
        /// <param name="strMensajeCifradoSalida"></param>
        /// <returns></returns>
        public int fCifrarMensaje_3DES(string strCodigoClaveDES, string strMensaje, ref string strMensajeCifradoSalida)
        {

            int codigoRetorno = 0;

            try
            {
                codigoRetorno = cCifrarMensaje_3DES.fCifrarMensaje_3DES(strCodigoClaveDES, strMensaje, ref strMensajeCifradoSalida);
            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }


            return codigoRetorno;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="strCodigoClaveDES"></param>
        /// <param name="strMensajeCifrado"></param>
        /// <param name="strMensajeSalida"></param>
        /// <returns></returns>
        public int fDescifrarMensaje_3DES(string strCodigoClaveDES, string strMensajeCifrado,
                                          ref string strMensajeSalida)
        {

            int codigoRetorno = 0;
            try
            {
                codigoRetorno = cDescifrarMensaje_3DES.fDescifrarMensaje_3DES(strCodigoClaveDES, strMensajeCifrado,
                                                                              ref strMensajeSalida);
            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }


            return codigoRetorno;
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intNumeroClave"></param>
        /// <param name="intIndiceClaveHSM"></param>
        /// <returns></returns>
        public int fCargarClavePrivada_RSA(string strCodigoInstitucion, int intNumeroClave, int intIndiceClaveHSM)
        {
            int codigoRetorno = 0;

            try
            {

                codigoRetorno = cCargarClavePrivada_RSA.fCargarClavePrivada_RSA(strCodigoInstitucion, intNumeroClave, intIndiceClaveHSM);
            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }

            return codigoRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intMetodoHash"></param>
        /// <param name="strDataEntrada"></param>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intNumeroClave"></param>
        /// <param name="strDataSalida"></param>
        /// <returns></returns>
        public int fGeneraFirma_RSA(int intMetodoHash, string strDataEntrada, string strCodigoInstitucion,
                                   int intNumeroClave, ref string strDataSalida)
        {
            int codigoRetorno = 0;

            try
            {
                codigoRetorno = cGeneraFirma_RSA.fGeneraFirma_RSA(intMetodoHash, strDataEntrada, strCodigoInstitucion,
                                                                  intNumeroClave, ref strDataSalida);
            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }


            return codigoRetorno;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="intMetodoHash"></param>
        /// <param name="strDataEntrada"></param>
        /// <param name="strFirmaEntrada"></param>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intIndiceClave"></param>
        /// <returns></returns>
        public int fValidaFirma_RSA(int intMetodoHash, string strDataEntrada, string strFirmaEntrada,
                                  string strCodigoInstitucion, int intIndiceClave)
        {
            int codigoRetorno = 0;

            try
            {
                codigoRetorno = cValidaFirma_RSA.fValidaFirma_RSA(intMetodoHash, strDataEntrada, strFirmaEntrada,
                                                                  strCodigoInstitucion, intIndiceClave);
            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }


            return codigoRetorno;
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="strCodigoInstitucionLocal"></param>
        /// <param name="strCodigoClaveDESLocal"></param>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intIndiceClavePublica"></param>
        /// <param name="strMensajeCifradoSalida"></param>
        /// <param name="strClaveDESCifradoSalida"></param>
        /// <returns></returns>
        public int fGeneraMensajeAutenticacion_3DES_RSA(string strCodigoInstitucionLocal, string strCodigoClaveDESLocal,
                                                      string strCodigoInstitucion, int intIndiceClavePublica,
                                                      ref string strMensajeCifradoSalida, ref string strClaveDESCifradoSalida)
        {

            int codigoRetorno = 0;

            try
            {
                codigoRetorno = cGeneraMensajeAutenticacion_3DES_RSA.fGeneraMensajeAutenticacion_3DES_RSA(strCodigoInstitucionLocal,
                                                                                                          strCodigoClaveDESLocal,
                                                                                                          strCodigoInstitucion,
                                                                                                          intIndiceClavePublica,
                                                                                                          ref strMensajeCifradoSalida,
                                                                                                          ref strClaveDESCifradoSalida);
            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }


            return codigoRetorno;
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="strMensajeCifrado"></param>
        /// <param name="strClaveDESCifrada"></param>
        /// <param name="strCodigoBinDEK"></param>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intNumeroClave"></param>
        /// <param name="strCodigoInstitucionOrigen"></param>
        /// <returns></returns>
        public int fValidaMensajeAutenticacion_3DES_RSA(string strMensajeCifrado, string strClaveDESCifrada,
                                                     string strCodigoBinDEK, string strCodigoInstitucion,
                                                     int intNumeroClave, string strCodigoInstitucionOrigen)
        {

            int codigoRetorno = 0;
            try
            {
                codigoRetorno = cValidaMensajeAutenticacion_3DES_RSA.fValidaMensajeAutenticacion_3DES_RSA(strMensajeCifrado, strClaveDESCifrada,
                                                         strCodigoBinDEK, strCodigoInstitucion, intNumeroClave, strCodigoInstitucionOrigen);

            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }


            return codigoRetorno;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="strMensaje"></param>
        /// <param name="strCodigoInstitucionDestino"></param>
        /// <param name="intIndiceClavePublicaDestino"></param>
        /// <param name="strMensajeCifradoSalida"></param>
        /// <param name="strClaveDESCifradaSalida"></param>
        /// <returns></returns>
        public int fCifrarMensaje_3DES_RSA(string strMensaje, string strCodigoInstitucionDestino,
                                         int intIndiceClavePublicaDestino, ref string strMensajeCifradoSalida,
                                         ref string strClaveDESCifradaSalida)
        {
            int codigoRetorno = 0;

            try
            {
                codigoRetorno = cCifrarMensaje_3DES_RSA.fCifrarMensaje_3DES_RSA(strMensaje, strCodigoInstitucionDestino,
                                                             intIndiceClavePublicaDestino, ref strMensajeCifradoSalida,
                                                             ref strClaveDESCifradaSalida);
            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }


            
            return codigoRetorno;

        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="strMensajeCifrado"></param>
        /// <param name="strClaveDESCifrada"></param>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intNumeroClave"></param>
        /// <param name="strMensajeDescifradoSalida"></param>
        /// <returns></returns>
        public int fDescifrarMensaje_3DES_RSA(string strMensajeCifrado, string strClaveDESCifrada,
                                           string strCodigoInstitucion, int intNumeroClave,
                                           ref string strMensajeDescifradoSalida)
        {

            int codigoRetorno = 0;

            try
            {
                codigoRetorno = cDescifrarMensaje_3DES_RSA.fDescifrarMensaje_3DES_RSA(strMensajeCifrado, strClaveDESCifrada,
                                                                                      strCodigoInstitucion, intNumeroClave,
                                                                                      ref strMensajeDescifradoSalida);
            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }


            return codigoRetorno;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intCodigoError"></param>
        /// <param name="strDescripcionError"></param>
        /// <returns></returns>
        public int fObtenerDescripcionError(int intCodigoError, ref string strDescripcionError)
        {
            int codigoRetorno = 0;

            try
            {
                codigoRetorno = cError.fObtenerDescripcionError(intCodigoError, ref strDescripcionError);
            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }


            return codigoRetorno;

        }


        public int fObtenerDescripcionError(int intCodigoError, ref int intOrigen, ref int intNivel, ref string strDescripcionError)
        {
            int codigoRetorno = 0;

            try
            {
                codigoRetorno = cError.fObtenerDescripcionError(intCodigoError, ref intOrigen, ref intNivel, ref strDescripcionError);
            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }


            return codigoRetorno;

        }

        ///////////////////////////////////////////////////////////////////////////////////////////
        // 
        // Lista de Funciones Historicas
        //
        ///////////////////////////////////////////////////////////////////////////////////////////


        /// <summary>
        /// 
        /// </summary>
        /// <param name="strCodigoClaveDES"></param>
        /// <param name="strMensaje"></param>
        /// <param name="strFechayyyymmddhhmmss"></param>
        /// <param name="strMensajeCifradoSalida"></param>
        /// <returns></returns>
        public int fCifrarMensaje_3DES_TH(string strCodigoClaveDES, string strMensaje, string strFechayyyymmddhhmmss, ref string strMensajeCifradoSalida)
        {

            int codigoRetorno = 0;

            try
            {
                codigoRetorno = cCifrarMensaje_3DES.fCifrarMensaje_3DES_TH(strCodigoClaveDES, strMensaje, strFechayyyymmddhhmmss, ref strMensajeCifradoSalida);
            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }


            return codigoRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strCodigoClaveDES"></param>
        /// <param name="strMensajeCifrado"></param>
        /// <param name="strFechayyyymmddhhmmss"></param>
        /// <param name="strMensajeSalida"></param>
        /// <returns></returns>
        public int fDescifrarMensaje_3DES_TH(string strCodigoClaveDES, string strMensajeCifrado,
                                             string strFechayyyymmddhhmmss, ref string strMensajeSalida)
        {

            int codigoRetorno = 0;
            try
            {
                codigoRetorno = cDescifrarMensaje_3DES.fDescifrarMensaje_3DES_TH(strCodigoClaveDES, strMensajeCifrado,
                                                                              strFechayyyymmddhhmmss, ref strMensajeSalida);
            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }


            return codigoRetorno;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="intMetodoHash"></param>
        /// <param name="strDataEntrada"></param>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intNumeroClave"></param>
        /// <param name="strFechayyyymmddhhmmss"></param>
        /// <param name="strDataSalida"></param>
        /// <returns></returns>
        public int fGeneraFirma_RSA_TH(int intMetodoHash, string strDataEntrada, string strCodigoInstitucion,
                                   int intNumeroClave, string strFechayyyymmddhhmmss, ref string strDataSalida)
        {
            int codigoRetorno = 0;

            try
            {
                codigoRetorno = cGeneraFirma_RSA.fGeneraFirma_RSA_TH(intMetodoHash, strDataEntrada, strCodigoInstitucion,
                                                                  intNumeroClave, strFechayyyymmddhhmmss, ref strDataSalida);
            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }


            return codigoRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intMetodoHash"></param>
        /// <param name="strDataEntrada"></param>
        /// <param name="strFirmaEntrada"></param>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intIndiceClave"></param>
        /// <param name="strFechayyyymmddhhmmss"></param>
        /// <returns></returns>
        public int fValidaFirma_RSA_TH(int intMetodoHash, string strDataEntrada, string strFirmaEntrada,
                                       string strCodigoInstitucion, int intIndiceClave, string strFechayyyymmddhhmmss)
        {
            int codigoRetorno = 0;

            try
            {
                codigoRetorno = cValidaFirma_RSA.fValidaFirma_RSA_TH(intMetodoHash, strDataEntrada, strFirmaEntrada,
                                                                  strCodigoInstitucion, intIndiceClave, strFechayyyymmddhhmmss);
            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }

            return codigoRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strCodigoInstitucionLocal"></param>
        /// <param name="strCodigoClaveDESLocal"></param>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intIndiceClavePublica"></param>
        /// <param name="strFechayyyymmddhhmmss"></param>
        /// <param name="strMensajeCifradoSalida"></param>
        /// <param name="strClaveDESCifradoSalida"></param>
        /// <returns></returns>
        public int fGeneraMensajeAutenticacion_3DES_RSA_TH(string strCodigoInstitucionLocal, string strCodigoClaveDESLocal,
                                                        string strCodigoInstitucion, int intIndiceClavePublica,
                                                        string strFechayyyymmddhhmmss,
                                                        ref string strMensajeCifradoSalida, ref string strClaveDESCifradoSalida)
        {
            int codigoRetorno = 0;

            try
            {
                codigoRetorno = cGeneraMensajeAutenticacion_3DES_RSA.fGeneraMensajeAutenticacion_3DES_RSA_TH(strCodigoInstitucionLocal,
                                                                                                          strCodigoClaveDESLocal,
                                                                                                          strCodigoInstitucion,
                                                                                                          intIndiceClavePublica,
                                                                                                          strFechayyyymmddhhmmss,
                                                                                                          ref strMensajeCifradoSalida,
                                                                                                          ref strClaveDESCifradoSalida);
            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }


            return codigoRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strMensajeCifrado"></param>
        /// <param name="strClaveDESCifrada"></param>
        /// <param name="strCodigoBinDEK"></param>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intNumeroClave"></param>
        /// <param name="strCodigoInstitucionOrigen"></param>
        /// <param name="strFechayyyymmddhhmmss"></param>
        /// <returns></returns>
        public int fValidaMensajeAutenticacion_3DES_RSA_TH(string strMensajeCifrado, string strClaveDESCifrada,
                                                           string strCodigoBinDEK, string strCodigoInstitucion,
                                                           int intNumeroClave, string strCodigoInstitucionOrigen,
                                                           string strFechayyyymmddhhmmss)
        {
            int codigoRetorno = 0;

            try
            {
                codigoRetorno = cValidaMensajeAutenticacion_3DES_RSA.fValidaMensajeAutenticacion_3DES_RSA_TH(strMensajeCifrado, strClaveDESCifrada,
                                                         strCodigoBinDEK, strCodigoInstitucion, intNumeroClave, strCodigoInstitucionOrigen,
                                                         strFechayyyymmddhhmmss);
            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }


            return codigoRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strMensaje"></param>
        /// <param name="strCodigoInstitucionDestino"></param>
        /// <param name="intIndiceClavePublicaDestino"></param>
        /// <param name="strFechayyyymmddhhmmss"></param>
        /// <param name="strMensajeCifradoSalida"></param>
        /// <param name="strClaveDESCifradaSalida"></param>
        /// <returns></returns>
        public int fCifrarMensaje_3DES_RSA_TH(string strMensaje, string strCodigoInstitucionDestino,
                                              int intIndiceClavePublicaDestino, string strFechayyyymmddhhmmss,
                                              ref string strMensajeCifradoSalida, ref string strClaveDESCifradaSalida)
        {
            int codigoRetorno = 0;

            try
            {
                codigoRetorno = cCifrarMensaje_3DES_RSA.fCifrarMensaje_3DES_RSA_TH(strMensaje, strCodigoInstitucionDestino,
                                                             intIndiceClavePublicaDestino, strFechayyyymmddhhmmss, ref strMensajeCifradoSalida,
                                                             ref strClaveDESCifradaSalida);
            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }

            return codigoRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strMensajeCifrado"></param>
        /// <param name="strClaveDESCifrada"></param>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intNumeroClave"></param>
        /// <param name="strFechayyyymmddhhmmss"></param>
        /// <param name="strMensajeDescifradoSalida"></param>
        /// <returns></returns>
        public int fDescifrarMensaje_3DES_RSA_TH(string strMensajeCifrado, string strClaveDESCifrada,
                                                 string strCodigoInstitucion, int intNumeroClave,
                                                 string strFechayyyymmddhhmmss, ref string strMensajeDescifradoSalida)
        {
            int codigoRetorno = 0;

            try
            {
                codigoRetorno = cDescifrarMensaje_3DES_RSA.fDescifrarMensaje_3DES_RSA_TH(strMensajeCifrado, strClaveDESCifrada,
                                                                                      strCodigoInstitucion, intNumeroClave,
                                                                                      strFechayyyymmddhhmmss,
                                                                                      ref strMensajeDescifradoSalida);

            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }

            return codigoRetorno;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strServidor"></param>
        /// <param name="strBaseDatos"></param>
        /// <param name="strUsuarioBD"></param>
        /// <param name="strClaveBD"></param>
        /// <returns></returns>
        public int fConectar_BaseDatos(string strServidor, string strBaseDatos, string strUsuarioBD, string strClaveBD)
        {

            int codigoRetorno = 0;

            dbname = strBaseDatos;
            dbserver = strServidor;
            dbuser = strUsuarioBD;
            dbpass = strClaveBD;

            try
            {
                codigoRetorno = cConectarBD.fConectarBD(strServidor, strBaseDatos, strUsuarioBD, strClaveBD);

            }
            catch (EntryPointNotFoundException)
            {
                codigoRetorno = 201;
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                codigoRetorno = 202;
            }
            catch (ExecutionEngineException)
            {
                codigoRetorno = 203;
            }

            return codigoRetorno;

        }

    }
}

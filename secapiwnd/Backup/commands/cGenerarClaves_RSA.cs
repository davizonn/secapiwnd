using System;
using System.Collections.Generic;
using System.Text;

namespace secapiwnd.commands
{
    public static class cGenerarClaves_RSA
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intLongitudClavePrivada"></param>
        /// <param name="intLongitudExponenteClavePublica"></param>
        /// <param name="lngExponenteClavePublica"></param>
        /// <param name="strClavePublicaCifradaSalida"></param>
        /// <param name="strClavePrivadaCifradaSalida"></param>
        /// <returns></returns>
        public static int fGenerarClaves_RSA(int intLongitudClavePrivada, int intLongitudExponenteClavePublica,
                                      long lngExponenteClavePublica, ref string strClavePublicaCifradaSalida,
                                      ref string strClavePrivadaCifradaSalida)
        {

            int codigoRetorno = 0;
            int nPrivKeyLength;
            int nExpPublicLength;
            uint nExpPublic;
            uint nPublicKeyLength;
            uint nPrivateKeyLength;
            StringBuilder szPublicKey = new StringBuilder(2048);
            StringBuilder szPrivateKey = new StringBuilder(2048);
            //string szPrivateKey = string.Empty;

            nPrivKeyLength = intLongitudClavePrivada;
            nExpPublicLength = 0;
            nExpPublic = 0; 

            codigoRetorno = cHeader.apissechsm_generate_rsa_key(nPrivKeyLength, nExpPublicLength, nExpPublic,
                                                        out nPublicKeyLength, szPublicKey, out nPrivateKeyLength,
                                                        szPrivateKey);

            strClavePublicaCifradaSalida = szPublicKey.ToString();
            strClavePrivadaCifradaSalida = szPrivateKey.ToString();

            
            strClavePublicaCifradaSalida = strClavePublicaCifradaSalida.Substring(0, (int)nPublicKeyLength);
            strClavePrivadaCifradaSalida = strClavePrivadaCifradaSalida.Substring(0, (int)nPrivateKeyLength);


            return codigoRetorno;

        }
    }
}

using System;
using System.Collections.Generic;
using System.Data;
//using System.Data.SqlClient;
using System.Text;

namespace secapiwnd.commands
{
    public static class cError
    {

        /// <summary>
        /// 
        /// </summary>
        //static SqlConnection objCon;
        //static SqlDataAdapter objDap;
        //static SqlCommand objCmd = new SqlCommand();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strServidor"></param>
        /// <param name="strBaseDatos"></param>
        /// <param name="strUsuarioBD"></param>
        /// <param name="strClaveBD"></param>
        /// <returns></returns>
        
        //private static int fObtenerConexionBD(string strServidor, string strBaseDatos, string strUsuarioBD, string strClaveBD)
        //{
        //    int codigoRetorno = 0;
        //    System.Text.StringBuilder stbConexion = new System.Text.StringBuilder();

        //    stbConexion.Append("server=");
        //    stbConexion.Append(strServidor);
        //    stbConexion.Append(";database=");
        //    stbConexion.Append(strBaseDatos);
        //    stbConexion.Append(";uid=");
        //    stbConexion.Append(strUsuarioBD);
        //    stbConexion.Append(";pwd=");
        //    stbConexion.Append(strClaveBD);

        //    objCon = new SqlConnection(stbConexion.ToString());
        //    if (objCon.State == ConnectionState.Closed)
        //    {
        //        codigoRetorno = 300;
        //    }

        //    return codigoRetorno;
        //}
        
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="intCodigoError"></param>
        /// <param name="strDescripcionError"></param>
        /// <returns></returns>

        //private static int fObtenerDescripcionErrorBD(int intCodigoError, ref string strDescripcionError)
        //{
        //    int codigoRetorno = 0;            
        //    string localDescripcionError = "Error: Codigo de error no registrado en la base de datos";

        //    //if (intCodigoError == -1)
        //    //{
        //    //    strDescripcionError = "No se puede encontrar el punto de entrada denominado 'apissechsm_encrypt_3des_rsa_message_th' en el archivo DLL 'Apissechsm.dll'.";
        //    //}
        //    //else
        //    //{
        //        try
        //        {
        //            objCmd.Connection = objCon;
        //            objCmd.CommandType = CommandType.Text;
        //            string sqlQuery = "SELECT  ERR_DESCRIPCION " +
        //                                               "FROM    TP_ERROR    " +
        //                                               "WHERE   ERR_COD = " + intCodigoError.ToString();

        //            objCmd.CommandText = sqlQuery;
        //            objCmd.CommandTimeout = 10;
        //            objCon.Open();
        //            SqlDataReader reader = objCmd.ExecuteReader();
        //            reader.Read();
        //            if (reader.HasRows == true)
        //            {
        //                strDescripcionError = reader[0].ToString();
        //            }
        //            else
        //            {
        //                strDescripcionError = localDescripcionError;
        //                codigoRetorno = 300;
        //            }

        //            //while (reader.Read())
        //            //{
        //            //    strDescripcionError = reader[0].ToString();
        //            //}
        //        }
        //        catch (Exception ex)
        //        {
        //            strDescripcionError = localDescripcionError;
        //            codigoRetorno = 200;
        //        }

        //    //}

        //    return codigoRetorno;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intCodigoError"></param>
        /// <param name="strDescripcionError"></param>
        /// <returns></returns>
        public static int fObtenerDescripcionError(int intCodigoError, ref string strDescripcionError)
        {
            int codigoRetorno = 0;
            string localDescripcionError = "Codigo de error no registrado en la base de datos";
            int nCodeError;
            uint nSourceOut;
            uint nLevelOut;
            StringBuilder szDescriptionOut = new StringBuilder(512);

            nCodeError = intCodigoError;

            try
            {
                codigoRetorno = cHeader.apissechsm_get_error_message(nCodeError, out nSourceOut, out nLevelOut, szDescriptionOut);

                if (codigoRetorno == 0)
                {
                    strDescripcionError = szDescriptionOut.ToString();
                }
                else
                {
                    strDescripcionError = localDescripcionError;
                    codigoRetorno = 300;
                }
            }
            catch (Exception ex)
            {
                strDescripcionError = localDescripcionError;
                codigoRetorno = 200;
            }

            return codigoRetorno;
        }

        public static int fObtenerDescripcionError(int intCodigoError, ref int intOrigen, ref int intNivel, ref string strDescripcionError)
        {
            int codigoRetorno = 0;
            string localDescripcionError = "Codigo de error no registrado en la base de datos";
            int nCodeError;
            uint nSourceOut;
            uint nLevelOut;
            StringBuilder szDescriptionOut = new StringBuilder(512);

            nCodeError = intCodigoError;

            try
            {
                codigoRetorno = cHeader.apissechsm_get_error_message(nCodeError, out nSourceOut, out nLevelOut, szDescriptionOut);

                if (codigoRetorno == 0)
                {
                    strDescripcionError = szDescriptionOut.ToString();
                    intOrigen = (int)nSourceOut;
                    intNivel = (int)nLevelOut;
                }
                else
                {
                    strDescripcionError = localDescripcionError;
                    intOrigen = 0;
                    intNivel = 0;
                    codigoRetorno = 300;
                }
            }
            catch (Exception ex)
            {
                strDescripcionError = localDescripcionError;
                codigoRetorno = 200;
            }

            return codigoRetorno;
        }

    }
}

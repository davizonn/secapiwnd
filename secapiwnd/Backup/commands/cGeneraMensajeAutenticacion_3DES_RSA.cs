using System;
using System.Collections.Generic;
using System.Text;

namespace secapiwnd.commands
{
    public static class cGeneraMensajeAutenticacion_3DES_RSA
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strCodigoInstitucionLocal"></param>
        /// <param name="strCodigoClaveDESLocal"></param>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intIndiceClavePublica"></param>
        /// <param name="strMensajeCifradoSalida"></param>
        /// <param name="strClaveDESCifradoSalida"></param>
        /// <returns></returns>
        public static int fGeneraMensajeAutenticacion_3DES_RSA(string strCodigoInstitucionLocal, string strCodigoClaveDESLocal,
                                                       string strCodigoInstitucion, int intIndiceClavePublica,
                                                       ref string strMensajeCifradoSalida, ref string strClaveDESCifradoSalida)
        {
            int codigoRetorno = 0;

            string szSourceInstitutionCode = strCodigoInstitucionLocal;
            string szSourceDESKeyCode = strCodigoClaveDESLocal;
            string szInstitutionCode = strCodigoInstitucion;
            int nPublicKeyIndex = intIndiceClavePublica;

            int nLenProtectedMessageOut = 0;
            StringBuilder szProtectedMessageOut = new StringBuilder(7400);
            int nLenProtectedDESKeyOut = 0;
            StringBuilder szProtectedDESKeyOut = new StringBuilder(2048);

            codigoRetorno = cHeader.apissechsm_generate_rsa_auth_message(szSourceInstitutionCode,
                                                                         szSourceDESKeyCode,
                                                                         szInstitutionCode,
                                                                         nPublicKeyIndex,
                                                                         out nLenProtectedMessageOut,
                                                                         szProtectedMessageOut,
                                                                         out nLenProtectedDESKeyOut,
                                                                         szProtectedDESKeyOut);

            strMensajeCifradoSalida = szProtectedMessageOut.ToString();
            strMensajeCifradoSalida = strMensajeCifradoSalida.Substring(0, (int)nLenProtectedMessageOut);
            strClaveDESCifradoSalida = szProtectedDESKeyOut.ToString();
            strClaveDESCifradoSalida = strClaveDESCifradoSalida.Substring(0, (int)nLenProtectedDESKeyOut);

            return codigoRetorno;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strCodigoInstitucionLocal"></param>
        /// <param name="strCodigoClaveDESLocal"></param>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intIndiceClavePublica"></param>
        /// <param name="strFechayyyymmddhhmmss"></param>
        /// <param name="strMensajeCifradoSalida"></param>
        /// <param name="strClaveDESCifradoSalida"></param>
        /// <returns></returns>
        public static int fGeneraMensajeAutenticacion_3DES_RSA_TH(string strCodigoInstitucionLocal, string strCodigoClaveDESLocal,
                                                        string strCodigoInstitucion, int intIndiceClavePublica,
                                                        string strFechayyyymmddhhmmss,
                                                        ref string strMensajeCifradoSalida, ref string strClaveDESCifradoSalida)
        {
            int codigoRetorno = 0;

            string szSourceInstitutionCode = strCodigoInstitucionLocal;
            string szSourceDESKeyCode = strCodigoClaveDESLocal;
            string szInstitutionCode = strCodigoInstitucion;
            int nPublicKeyIndex = intIndiceClavePublica;
            string szFechayyyymmddhhmmss = strFechayyyymmddhhmmss;

            int nLenProtectedMessageOut = 0;
            StringBuilder szProtectedMessageOut = new StringBuilder(7400);
            int nLenProtectedDESKeyOut = 0;
            StringBuilder szProtectedDESKeyOut = new StringBuilder(2048);

            codigoRetorno = cHeader.apissechsm_generate_rsa_auth_message_th(szSourceInstitutionCode,
                                                                         szSourceDESKeyCode,
                                                                         szInstitutionCode,
                                                                         nPublicKeyIndex,
                                                                         szFechayyyymmddhhmmss,
                                                                         out nLenProtectedMessageOut,
                                                                         szProtectedMessageOut,
                                                                         out nLenProtectedDESKeyOut,
                                                                         szProtectedDESKeyOut);

            strMensajeCifradoSalida = szProtectedMessageOut.ToString();
            strMensajeCifradoSalida = strMensajeCifradoSalida.Substring(0, (int)nLenProtectedMessageOut);
            strClaveDESCifradoSalida = szProtectedDESKeyOut.ToString();
            strClaveDESCifradoSalida = strClaveDESCifradoSalida.Substring(0, (int)nLenProtectedDESKeyOut);

            return codigoRetorno;

        }
    }
}

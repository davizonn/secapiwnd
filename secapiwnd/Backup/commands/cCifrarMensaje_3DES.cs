using System;
using System.Collections.Generic;
using System.Text;

namespace secapiwnd.commands
{
    public static class cCifrarMensaje_3DES
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strCodigoClaveDES"></param>
        /// <param name="strMensaje"></param>
        /// <param name="strMensajeCifradoSalida"></param>
        /// <returns></returns>
        public static int fCifrarMensaje_3DES(string strCodigoClaveDES, string strMensaje, ref string strMensajeCifradoSalida)
        {
            int codigoRetorno = 0;

            string szDESKeyCode = strCodigoClaveDES;
            int nMessageLength = strMensaje.Length;
            string szMessage = strMensaje;

            int nEncryptedMessageLength = 0;
            StringBuilder szEncryptedMessage = new StringBuilder(7400);

            codigoRetorno = cHeader.apissechsm_encrypt_message_3des(szDESKeyCode, nMessageLength, szMessage,
                                                            out nEncryptedMessageLength, szEncryptedMessage);

            strMensajeCifradoSalida = szEncryptedMessage.ToString();
            strMensajeCifradoSalida = strMensajeCifradoSalida.Substring(0, (int)nEncryptedMessageLength);

            return codigoRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strCodigoClaveDES"></param>
        /// <param name="strMensaje"></param>
        /// <param name="strFechayyyymmddhhmmss"></param>
        /// <param name="strMensajeCifradoSalida"></param>
        /// <returns></returns>
        public static int fCifrarMensaje_3DES_TH(string strCodigoClaveDES, string strMensaje, 
                                                 string strFechayyyymmddhhmmss, ref string strMensajeCifradoSalida)
        {
            int codigoRetorno = 0;

            string szDESKeyCode = strCodigoClaveDES;
            int nMessageLength = strMensaje.Length;
            string szMessage = strMensaje;
            string szFechayyyymmddhhmmss = strFechayyyymmddhhmmss;

            int nEncryptedMessageLength = 0;
            StringBuilder szEncryptedMessage = new StringBuilder(7400);

            codigoRetorno = cHeader.apissechsm_encrypt_message_3des_th(szDESKeyCode, 
                                                                       nMessageLength, 
                                                                       szMessage,
                                                                       szFechayyyymmddhhmmss,
                                                                       out nEncryptedMessageLength, 
                                                                       szEncryptedMessage);

            strMensajeCifradoSalida = szEncryptedMessage.ToString();
            strMensajeCifradoSalida = strMensajeCifradoSalida.Substring(0, (int)nEncryptedMessageLength);

            return codigoRetorno;
        }

    }
}

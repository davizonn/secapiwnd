using System;
using System.Collections.Generic;
using System.Text;

namespace secapiwnd.commands
{
    public static class cCargarClavePrivada_RSA
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intNumeroClave"></param>
        /// <param name="intIndiceClaveHSM"></param>
        /// <returns></returns>
        public static int fCargarClavePrivada_RSA(string strCodigoInstitucion, int intNumeroClave, int intIndiceClaveHSM)
        {
            int codigoRetorno = 0;

            int nKeyNumber = intNumeroClave;
            int nKeyIndex = intIndiceClaveHSM;
            StringBuilder szInstitutionCode;

            szInstitutionCode = new StringBuilder(strCodigoInstitucion);

            codigoRetorno = cHeader.apissechsm_load_rsa_priv_key(szInstitutionCode, nKeyNumber, nKeyIndex);

            return codigoRetorno;
        }

    }
}

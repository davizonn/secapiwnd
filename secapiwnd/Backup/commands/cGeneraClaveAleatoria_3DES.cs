using System;
using System.Collections.Generic;
using System.Text;

namespace secapiwnd.commands
{
    public static class cGeneraClaveAleatoria_3DES
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intTipoClave"></param>
        /// <param name="intTipoLongitudClave"></param>
        /// <param name="strClaveDESCifradaSalida"></param>
        /// <param name="strValorVerificacionClaveDESCifradaSalida"></param>
        /// <returns></returns>
        public static int fGeneraClaveAleatoria_3DES(int intTipoClave, int intTipoLongitudClave,
                                      ref string strClaveDESCifradaSalida,
                                      ref string strValorVerificacionClaveDESCifradaSalida)
        {
            int codigoRetorno = 0;

            int nKeyType = intTipoClave;
            int nLenType = intTipoLongitudClave;
            int nKeyLengthOut = 0;
            StringBuilder szDESKeyOut = new StringBuilder(2048);
            byte[] szCheckValueDESKeyOut = new byte[32];

            codigoRetorno = cHeader.apissechsm_generate_random_key_3des(nKeyType, nLenType,
                                                out nKeyLengthOut, szDESKeyOut, szCheckValueDESKeyOut);


            strClaveDESCifradaSalida = szDESKeyOut.ToString();
            strClaveDESCifradaSalida = strClaveDESCifradaSalida.Substring(0, (int)nKeyLengthOut);
            strValorVerificacionClaveDESCifradaSalida = System.Text.Encoding.ASCII.GetString(szCheckValueDESKeyOut); // szCheckValueDESKeyOut.ToString();

            return codigoRetorno;
        }

    }
}

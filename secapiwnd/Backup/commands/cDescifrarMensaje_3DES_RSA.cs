using System;
using System.Collections.Generic;
using System.Text;

namespace secapiwnd.commands
{
    public static class cDescifrarMensaje_3DES_RSA
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strMensajeCifrado"></param>
        /// <param name="strClaveDESCifrada"></param>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intNumeroClave"></param>
        /// <param name="strMensajeDescifradoSalida"></param>
        /// <returns></returns>
        public static int fDescifrarMensaje_3DES_RSA(string strMensajeCifrado, string strClaveDESCifrada,
                                            string strCodigoInstitucion, int intNumeroClave,
                                            ref string strMensajeDescifradoSalida)
        {
            int codigoRetorno = 0;

            int nLenProtectedMessageIn = strMensajeCifrado.Length;
            string szProtectedMessageIn = strMensajeCifrado;
            int nLenProtectedDESKeyIn = strClaveDESCifrada.Length;
            string szProtectedDESKeyIn = strClaveDESCifrada;
            string szInstitutionCode = strCodigoInstitucion;
            int nKeyNumber = intNumeroClave;

            uint nLenDecryptedMessageOut = 0;
            StringBuilder szDecryptedMessageOut = new StringBuilder(3700);

            codigoRetorno = cHeader.apissechsm_decrypt_3des_rsa_message(nLenProtectedMessageIn,
                                                                        szProtectedMessageIn,
                                                                        nLenProtectedDESKeyIn,
                                                                        szProtectedDESKeyIn, 
                                                                        szInstitutionCode,
                                                                        nKeyNumber,
                                                                        out nLenDecryptedMessageOut,
                                                                        szDecryptedMessageOut);

            strMensajeDescifradoSalida = szDecryptedMessageOut.ToString();
            strMensajeDescifradoSalida = strMensajeDescifradoSalida.Substring(0, (int)nLenDecryptedMessageOut);

            return codigoRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strMensajeCifrado"></param>
        /// <param name="strClaveDESCifrada"></param>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intNumeroClave"></param>
        /// <param name="strFechayyyymmddhhmmss"></param>
        /// <param name="strMensajeDescifradoSalida"></param>
        /// <returns></returns>
        public static int fDescifrarMensaje_3DES_RSA_TH(string strMensajeCifrado, string strClaveDESCifrada,
                                                 string strCodigoInstitucion, int intNumeroClave,
                                                 string strFechayyyymmddhhmmss, ref string strMensajeDescifradoSalida)
        {
            int codigoRetorno = 0;

            int nLenProtectedMessageIn = strMensajeCifrado.Length;
            string szProtectedMessageIn = strMensajeCifrado;
            int nLenProtectedDESKeyIn = strClaveDESCifrada.Length;
            string szProtectedDESKeyIn = strClaveDESCifrada;
            string szInstitutionCode = strCodigoInstitucion;
            int nKeyNumber = intNumeroClave;
            string szFechayyyymmddhhmmss = strFechayyyymmddhhmmss;

            uint nLenDecryptedMessageOut = 0;
            StringBuilder szDecryptedMessageOut = new StringBuilder(3700);

            codigoRetorno = cHeader.apissechsm_decrypt_3des_rsa_message_th(nLenProtectedMessageIn,
                                                                        szProtectedMessageIn,
                                                                        nLenProtectedDESKeyIn,
                                                                        szProtectedDESKeyIn,
                                                                        szInstitutionCode,
                                                                        nKeyNumber,
                                                                        szFechayyyymmddhhmmss,
                                                                        out nLenDecryptedMessageOut,
                                                                        szDecryptedMessageOut);

            strMensajeDescifradoSalida = szDecryptedMessageOut.ToString();
            strMensajeDescifradoSalida = strMensajeDescifradoSalida.Substring(0, (int)nLenDecryptedMessageOut);

            return codigoRetorno;
        }

    }
}

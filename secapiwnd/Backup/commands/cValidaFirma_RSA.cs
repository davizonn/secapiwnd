using System;
using System.Collections.Generic;
using System.Text;

namespace secapiwnd.commands
{
    public static class cValidaFirma_RSA
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="intMetodoHash"></param>
        /// <param name="strDataEntrada"></param>
        /// <param name="strFirmaEntrada"></param>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intIndiceClave"></param>
        /// <returns></returns>
        public static int fValidaFirma_RSA(int intMetodoHash, string strDataEntrada, string strFirmaEntrada,
                          string strCodigoInstitucion, int intIndiceClave)
        {

            int codigoRetorno = 0;

            int nHashMethodIn = intMetodoHash;
            int nLenDataSignedIn = strDataEntrada.Length;
            string szDataSignedIn = strDataEntrada;
            int nLenDataSignatureIn = strFirmaEntrada.Length;
            string szDataSignatureIn = strFirmaEntrada;
            string szSourceInstitutionCodeIn = strCodigoInstitucion;
            int nPublicKeyIndexIn = intIndiceClave;

            codigoRetorno = cHeader.apissechsm_validate_rsa_signature(nHashMethodIn,
                                                                      nLenDataSignedIn,
                                                                      szDataSignedIn,
                                                                      nLenDataSignatureIn,
                                                                      szDataSignatureIn,
                                                                      szSourceInstitutionCodeIn,
                                                                      nPublicKeyIndexIn);

            return codigoRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intMetodoHash"></param>
        /// <param name="strDataEntrada"></param>
        /// <param name="strFirmaEntrada"></param>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intIndiceClave"></param>
        /// <param name="strFechayyyymmddhhmmss"></param>
        /// <returns></returns>
        public static int fValidaFirma_RSA_TH(int intMetodoHash, string strDataEntrada, string strFirmaEntrada,
                                              string strCodigoInstitucion, int intIndiceClave, string strFechayyyymmddhhmmss)
        {
            int codigoRetorno = 0;

            int nHashMethodIn = intMetodoHash;
            int nLenDataSignedIn = strDataEntrada.Length;
            string szDataSignedIn = strDataEntrada;
            int nLenDataSignatureIn = strFirmaEntrada.Length;
            string szDataSignatureIn = strFirmaEntrada;
            string szSourceInstitutionCodeIn = strCodigoInstitucion;
            int nPublicKeyIndexIn = intIndiceClave;
            string szFechayyyymmddhhmmss = strFechayyyymmddhhmmss;

            codigoRetorno = cHeader.apissechsm_validate_rsa_signature_th(nHashMethodIn,
                                                                      nLenDataSignedIn,
                                                                      szDataSignedIn,
                                                                      nLenDataSignatureIn,
                                                                      szDataSignatureIn,
                                                                      szSourceInstitutionCodeIn,
                                                                      nPublicKeyIndexIn,
                                                                      szFechayyyymmddhhmmss);

            return codigoRetorno;
        }
    }
}

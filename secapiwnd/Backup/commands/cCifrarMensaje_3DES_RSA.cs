using System;
using System.Collections.Generic;
using System.Text;

namespace secapiwnd.commands
{
    public static class cCifrarMensaje_3DES_RSA
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strMensaje"></param>
        /// <param name="strCodigoInstitucionDestino"></param>
        /// <param name="intIndiceClavePublicaDestino"></param>
        /// <param name="strMensajeCifradoSalida"></param>
        /// <param name="strClaveDESCifradaSalida"></param>
        /// <returns></returns>
        public static int fCifrarMensaje_3DES_RSA(string strMensaje, string strCodigoInstitucionDestino,
                                         int intIndiceClavePublicaDestino, ref string strMensajeCifradoSalida,
                                         ref string strClaveDESCifradaSalida)
        {
            /* CRV 20110601 */
            int codigoRetorno = 0;

            int nLenMessageIn = strMensaje.Length;
            string szMessageIn = strMensaje;
            string szTargetInstitutionCodeIn = strCodigoInstitucionDestino;
            int nTargetPublicKeyIndexIn = intIndiceClavePublicaDestino;
            int nLenProtectedMessageOut = 0;
            StringBuilder szProtectedMessageOut = new StringBuilder(7400);
            int nLenProtectedDESKeyOut = 0;
            StringBuilder szProtectedDESKeyOut = new StringBuilder(2048);

            codigoRetorno = cHeader.apissechsm_encrypt_3des_rsa_message(nLenMessageIn,
                                                                        szMessageIn,
                                                                        szTargetInstitutionCodeIn,
                                                                        nTargetPublicKeyIndexIn,
                                                                        out nLenProtectedMessageOut,
                                                                        szProtectedMessageOut,
                                                                        out nLenProtectedDESKeyOut,
                                                                        szProtectedDESKeyOut);

            strMensajeCifradoSalida = szProtectedMessageOut.ToString();
            strMensajeCifradoSalida = strMensajeCifradoSalida.Substring(0, (int)nLenProtectedMessageOut);
            strClaveDESCifradaSalida = szProtectedDESKeyOut.ToString();
            strClaveDESCifradaSalida = strClaveDESCifradaSalida.Substring(0, nLenProtectedDESKeyOut);

            return codigoRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strMensaje"></param>
        /// <param name="strCodigoInstitucionDestino"></param>
        /// <param name="intIndiceClavePublicaDestino"></param>
        /// <param name="strFechayyyymmddhhmmss"></param>
        /// <param name="strMensajeCifradoSalida"></param>
        /// <param name="strClaveDESCifradaSalida"></param>
        /// <returns></returns>
        public static int fCifrarMensaje_3DES_RSA_TH(string strMensaje, string strCodigoInstitucionDestino,
                                              int intIndiceClavePublicaDestino, string strFechayyyymmddhhmmss,
                                              ref string strMensajeCifradoSalida, ref string strClaveDESCifradaSalida)
        {
            int codigoRetorno = 0;

            int nLenMessageIn = strMensaje.Length;
            string szMessageIn = strMensaje;
            string szTargetInstitutionCodeIn = strCodigoInstitucionDestino;
            int nTargetPublicKeyIndexIn = intIndiceClavePublicaDestino;
            string szFechayyyymmddhhmmss = strFechayyyymmddhhmmss;
            
            int nLenProtectedMessageOut = 0;
            StringBuilder szProtectedMessageOut = new StringBuilder(7400);
            int nLenProtectedDESKeyOut = 0;
            StringBuilder szProtectedDESKeyOut = new StringBuilder(2048);

            codigoRetorno = cHeader.apissechsm_encrypt_3des_rsa_message_th(nLenMessageIn,
                                                                        szMessageIn,
                                                                        szTargetInstitutionCodeIn,
                                                                        nTargetPublicKeyIndexIn,
                                                                        szFechayyyymmddhhmmss,
                                                                        out nLenProtectedMessageOut,
                                                                        szProtectedMessageOut,
                                                                        out nLenProtectedDESKeyOut,
                                                                        szProtectedDESKeyOut);

            strMensajeCifradoSalida = szProtectedMessageOut.ToString();
            strMensajeCifradoSalida = strMensajeCifradoSalida.Substring(0, (int)nLenProtectedMessageOut);
            strClaveDESCifradaSalida = szProtectedDESKeyOut.ToString();
            strClaveDESCifradaSalida = strClaveDESCifradaSalida.Substring(0, nLenProtectedDESKeyOut);

            return codigoRetorno;
        }


    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace secapiwnd.commands
{
    public static class cDescifrarMensaje_3DES
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strCodigoClaveDES"></param>
        /// <param name="strMensajeCifrado"></param>
        /// <param name="strMensajeSalida"></param>
        /// <returns></returns>
        public static int fDescifrarMensaje_3DES(string strCodigoClaveDES, string strMensajeCifrado, ref string strMensajeSalida)
        {

            int codigoRetorno = 0;

            string szDESKeyCode = strCodigoClaveDES;
            int nEncryptedMessageLength = strMensajeCifrado.Length;
            string szEncryptedMessage = string.Empty;

            int nMessageLengthOut = 0;
            StringBuilder szMessageOut = new StringBuilder(3700);


            szEncryptedMessage = strMensajeCifrado;

            codigoRetorno = cHeader.apissechsm_dencrypt_message_3des(szDESKeyCode,
                                                                     nEncryptedMessageLength,
                                                                     szEncryptedMessage,
                                                                     out nMessageLengthOut,
                                                                     szMessageOut);

            strMensajeSalida = szMessageOut.ToString();
            strMensajeSalida = strMensajeSalida.Substring(0, nMessageLengthOut);

            return codigoRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strCodigoClaveDES"></param>
        /// <param name="strMensajeCifrado"></param>
        /// <param name="strFechayyyymmddhhmmss"></param>
        /// <param name="strMensajeSalida"></param>
        /// <returns></returns>
        public static int fDescifrarMensaje_3DES_TH(string strCodigoClaveDES, string strMensajeCifrado,
                                                 string strFechayyyymmddhhmmss, ref string strMensajeSalida)
        {

            int codigoRetorno = 0;

            string szDESKeyCode = strCodigoClaveDES;
            int nEncryptedMessageLength = strMensajeCifrado.Length;
            string szEncryptedMessage = string.Empty;
            string szFechayyyymmddhhmmss = strFechayyyymmddhhmmss;

            int nMessageLengthOut = 0;
            StringBuilder szMessageOut = new StringBuilder(3700);


            szEncryptedMessage = strMensajeCifrado;

            codigoRetorno = cHeader.apissechsm_dencrypt_message_3des_th(szDESKeyCode,
                                                                     nEncryptedMessageLength,
                                                                     szEncryptedMessage,
                                                                     szFechayyyymmddhhmmss,
                                                                     out nMessageLengthOut,
                                                                     szMessageOut);

            strMensajeSalida = szMessageOut.ToString();
            strMensajeSalida = strMensajeSalida.Substring(0, nMessageLengthOut);

            return codigoRetorno;
        }


    }
}

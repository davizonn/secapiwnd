using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace secapiwnd.commands
{
    class Util
    {
        private const string NAME_FILE_APISSEC = "apissechsm.ini";
        private const string NAME_ENV_VAR = "APISSECHSM_DIR_LIC";

        private static string strConfigFile = "";
        public static string strWorkDir = "";

        [DllImport("KERNEL32.DLL", EntryPoint = "GetPrivateProfileStringW",
            SetLastError = true,
            CharSet = CharSet.Unicode, ExactSpelling = true,
            CallingConvention = CallingConvention.StdCall)]
        private static extern int GetPrivateProfileString(
            string lpAppName,
            string lpKeyName,
            string lpDefault,
            string lpReturnString,
            int nSize,
            string lpFilename);

        private static int GetConfigFilePath()
        {
            int rc = -1;
            string acCurrentDir;

            Log.Insertar(Log.DEB, "Obteniendo la ruta del archivo de configuracion");

            /* RUTA 1: Directorio Actual */
            // Obtenemos el directorio actual
            acCurrentDir = Directory.GetCurrentDirectory();
            Log.Insertar(Log.DEB, "CurrentDirectory [" + acCurrentDir + "]");

            // Contruimos el path del archivo de configuracion
            strConfigFile = acCurrentDir + @"\" + NAME_FILE_APISSEC;
            Log.Insertar(Log.DEB, "ConfigFile [" + strConfigFile + "]");

            // Comprobamos si existe el archivo de configuracion
            if (!File.Exists(strConfigFile))
                Log.Insertar(Log.FAI, "No existe el archivo de configuracion [" + strConfigFile + "]");
            else
                rc = 0;

            /* RUTA 2: Variable de entorno. */
            if (rc != 0)
            {
                Log.Insertar(Log.FAI, "Se buscara el valor de la variable de entorno [" + NAME_ENV_VAR + "]");

                try
                {
                    // Obtenemos variable de entorno NAME_ENV_VAR
                    acCurrentDir = System.Environment.GetEnvironmentVariable(NAME_ENV_VAR);
                    Log.Insertar(Log.DEB, "Se buscara en el directorio Novatronic [" + acCurrentDir + "]");

                    // Construimos la ruta del archivo de configuracion
                    strConfigFile = acCurrentDir + @"\" + NAME_FILE_APISSEC;
                    Log.Insertar(Log.DEB, "ConfigFile [" + strConfigFile + "]");

                    if (!File.Exists(strConfigFile))
                        Log.Insertar(Log.FAI, "No existe el archivo de configuracion [" + strConfigFile + "]");
                    else
                        rc = 0;
                }
                catch (Exception ex)
                {
                    Log.Insertar(Log.FAI, "No existe variable de entorno [" + NAME_ENV_VAR + "]");
                }
            }

            /* RUTA 3: Directorio Windows */
            if (rc != 0)
            {
                Log.Insertar(Log.FAI, "Se buscara en el directorio Windows");

                try
                {
                    // Obtenemos el directorio windows del sistema
                    acCurrentDir = System.Environment.GetEnvironmentVariable("WINDIR");

                    // Construimos el path del archivo de configuracion
                    strConfigFile = acCurrentDir + @"\" + NAME_FILE_APISSEC;
                    Log.Insertar(Log.DEB, "ConfigFile [" + strConfigFile + "]");

                    if (!File.Exists(strConfigFile))
                        Log.Insertar(Log.FAI, "No existe el archivo de configuracion [" + strConfigFile + "]");
                    else
                        rc = 0;
                }
                catch (Exception ex)
                {
                    Log.Insertar(Log.FAI, "No se puede buscar en carpeta Windows");
                }
            }

            /* RUTA 4: Disco C */
            if (rc != 0)
            {
                Log.Insertar(Log.FAI, "Se buscara en el directorio [C:\\]");

                // Construimos el path del archivo de configuracion
                strConfigFile = @"C:\" + NAME_FILE_APISSEC;
                Log.Insertar(Log.DEB, "ConfigFile [" + strConfigFile + "]");

                // Comprobamos si existe el archivo de configuracion
                if (!File.Exists(strConfigFile))
                    Log.Insertar(Log.FAI, "No existe el archivo de configuracion [" + strConfigFile + "]");
                else
                    rc = 0;
            }

            if (rc == 0)
                Log.Insertar(Log.DEB, "Archivo de configuracion [" + strConfigFile + "]");
            else
            {
                Log.Insertar(Log.FAI, "ERROR: No se encontro el archivo de configuracion");
                strConfigFile = "";
            }

            return rc;
        }

        private static string ReadIniEntry(string entry, string key)
        {
            string returnString = new string(' ', 1024);
            GetPrivateProfileString(entry, key, "", returnString, 1024, strConfigFile);

            return returnString.Split('\0')[0];
        }

        public static void InicializarRuta()
        {
            if (GetConfigFilePath() != 0)
                strWorkDir = @"C:\TEMP";
            else
                strWorkDir = ReadIniEntry("General", "Directorio");
        }

        public static string getVariableEntorno(){
            string acCurrentDir = System.Environment.GetEnvironmentVariable(NAME_ENV_VAR);
            return acCurrentDir;
        }

        public static bool CargarLibreria()
        {
            return CargarLibreria(cHeader.nomDLL);
        }

        internal static bool CargarLibreria(string nomDLL)
        {
            int numError;
            bool isError = false;

            Log.Insertar(Log.DEB, "Cargando librer�a [" + nomDLL + "]");

            if (cHeader.hModule.Equals(IntPtr.Zero))
            {
                if (File.Exists(nomDLL))
                {
                    cHeader.hModule = cHeader.LoadLibraryEx(nomDLL, IntPtr.Zero, cHeader.LoadLibraryFlags.LOAD_WITH_ALTERED_SEARCH_PATH);
                    numError = Marshal.GetLastWin32Error();
                    
                    if (cHeader.hModule.Equals(IntPtr.Zero))
                    {
                        cHeader.numError = numError;
                        Log.Insertar(Log.ERR, "ERROR: [" + numError + "] [" + cHeader.GetErrorMessage(numError) + "]");
                        isError = true;
                    }
                    else
                    {
                        Log.Insertar(Log.DEB, "Libreria cargada correctamente [" + cHeader.hModule.ToString() + "]");
                    }
                }
                else
                {
                    Log.Insertar(Log.FAI, "ERROR: No existe librer�a " + nomDLL);
                    isError = true;
                }
            }
            else
            {
                Log.Insertar(Log.DEB, "Librer�a ya se encuetra cargada");
            }

            if (isError)
            {
                Log.Insertar(Log.FAI, "ERROR: No se ha podido cargar librer�a " + nomDLL);
            }
            return !isError;
        }
    }
}

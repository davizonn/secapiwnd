using System;
using System.Collections.Generic;
using System.Text;

namespace secapiwnd.commands
{
    public static class cConectarBD
    {

        public static int fConectarBD(string strServidor, string strBaseDatos, string strUsuarioBD, string strClaveBD)
        {
            int codigoRetorno = 0;

            string szServer = strServidor;
            string szDatabase = strBaseDatos;
            string szDbUser = strUsuarioBD;
            string szDbPassword = strClaveBD;

            codigoRetorno = cHeader.apissechsm_db_connect(szServer, szDatabase, szDbUser, szDbPassword);

            return codigoRetorno;
        }

    }
}

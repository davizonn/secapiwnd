using System;
using System.Collections.Generic;
using System.Text;

namespace secapiwnd.commands
{
    public static class cValidaMensajeAutenticacion_3DES_RSA
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strMensajeCifrado"></param>
        /// <param name="strClaveDESCifrada"></param>
        /// <param name="strCodigoBinDEK"></param>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intNumeroClave"></param>
        /// <param name="strCodigoInstitucionOrigen"></param>
        /// <returns></returns>
        public static int fValidaMensajeAutenticacion_3DES_RSA(string strMensajeCifrado, string strClaveDESCifrada,
                                                     string strCodigoBinDEK, string strCodigoInstitucion,
                                                     int intNumeroClave, string strCodigoInstitucionOrigen)
        {
            int codigoRetorno = 0;
            int nLenProtectedMessage = 0;
            string szProtectedMessage = string.Empty;
            int nLenProtectedDESKey = 0;
            string szProtectedDESKey = string.Empty;
            string szDESKeyCode = string.Empty;
            string szInstitutionCode = string.Empty;
            int nKeyNumber = 0;
            string szSourceInstitutionCode = string.Empty;

            nLenProtectedMessage = strMensajeCifrado.Length;
            szProtectedMessage = strMensajeCifrado;
            nLenProtectedDESKey = strClaveDESCifrada.Length;
            szProtectedDESKey = strClaveDESCifrada;
            szDESKeyCode = strCodigoBinDEK;
            szInstitutionCode = strCodigoInstitucion;
            nKeyNumber = intNumeroClave;
            szSourceInstitutionCode = strCodigoInstitucionOrigen;
            
            codigoRetorno = cHeader.apissechsm_validate_rsa_auth_message(nLenProtectedMessage,
                                                                         szProtectedMessage,
                                                                         nLenProtectedDESKey,
                                                                         szProtectedDESKey,
                                                                         szDESKeyCode,
                                                                         szInstitutionCode,
                                                                         nKeyNumber,
                                                                         szSourceInstitutionCode);

            return codigoRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strMensajeCifrado"></param>
        /// <param name="strClaveDESCifrada"></param>
        /// <param name="strCodigoBinDEK"></param>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intNumeroClave"></param>
        /// <param name="strCodigoInstitucionOrigen"></param>
        /// <param name="strFechayyyymmddhhmmss"></param>
        /// <returns></returns>
        public static int fValidaMensajeAutenticacion_3DES_RSA_TH(string strMensajeCifrado, string strClaveDESCifrada,
                                                           string strCodigoBinDEK, string strCodigoInstitucion,
                                                           int intNumeroClave, string strCodigoInstitucionOrigen,
                                                           string strFechayyyymmddhhmmss)
        {

            int codigoRetorno = 0;
            int nLenProtectedMessage = 0;
            string szProtectedMessage = string.Empty;
            int nLenProtectedDESKey = 0;
            string szProtectedDESKey = string.Empty;
            string szDESKeyCode = string.Empty;
            string szInstitutionCode = string.Empty;
            int nKeyNumber = 0;
            string szSourceInstitutionCode = string.Empty;
            string szFechayyyymmddhhmmss = strFechayyyymmddhhmmss;

            nLenProtectedMessage = strMensajeCifrado.Length;
            szProtectedMessage = strMensajeCifrado;
            nLenProtectedDESKey = strClaveDESCifrada.Length;
            szProtectedDESKey = strClaveDESCifrada;
            szDESKeyCode = strCodigoBinDEK;
            szInstitutionCode = strCodigoInstitucion;
            nKeyNumber = intNumeroClave;
            szSourceInstitutionCode = strCodigoInstitucionOrigen;

            codigoRetorno = cHeader.apissechsm_validate_rsa_auth_message_th(nLenProtectedMessage,
                                                                         szProtectedMessage,
                                                                         nLenProtectedDESKey,
                                                                         szProtectedDESKey,
                                                                         szDESKeyCode,
                                                                         szInstitutionCode,
                                                                         nKeyNumber,
                                                                         szSourceInstitutionCode,
                                                                         szFechayyyymmddhhmmss);

            return codigoRetorno;

        }

    }
}

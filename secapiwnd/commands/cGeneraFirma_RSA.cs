using System;
using System.Collections.Generic;
using System.Text;

namespace secapiwnd.commands
{
    public static class cGeneraFirma_RSA
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intMetodoHash"></param>
        /// <param name="strDataEntrada"></param>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intNumeroClave"></param>
        /// <param name="strDataSalida"></param>
        /// <returns></returns>
        public static int fGeneraFirma_RSA(int intMetodoHash, string strDataEntrada, string strCodigoInstitucion,
                                   int intNumeroClave, ref string strDataSalida)
        {
            int codigoRetorno = 0;

            int nHashMethodIn = intMetodoHash;
            int nLenDataToSignIn = strDataEntrada.Length;
            string szDataToSignIn = strDataEntrada;
            string szInstitutionCode = strCodigoInstitucion;
            int nKeyNumber = intNumeroClave;

            int nLenDataSignatureOut = 0;
            StringBuilder szDataSignatureOut = new StringBuilder(2048);

            codigoRetorno = cHeader.apissechsm_generate_rsa_signature(nHashMethodIn,
                                                                      nLenDataToSignIn,
                                                                      szDataToSignIn,
                                                                      szInstitutionCode,
                                                                      nKeyNumber,
                                                                      out nLenDataSignatureOut,
                                                                      szDataSignatureOut);

            strDataSalida = szDataSignatureOut.ToString();
            strDataSalida = strDataSalida.Substring(0, nLenDataSignatureOut);

            return codigoRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intMetodoHash"></param>
        /// <param name="strDataEntrada"></param>
        /// <param name="strCodigoInstitucion"></param>
        /// <param name="intNumeroClave"></param>
        /// <param name="strFechayyyymmddhhmmss"></param>
        /// <param name="strDataSalida"></param>
        /// <returns></returns>
        public static int fGeneraFirma_RSA_TH(int intMetodoHash, string strDataEntrada, string strCodigoInstitucion,
                                   int intNumeroClave, string strFechayyyymmddhhmmss, ref string strDataSalida)
        {
            int codigoRetorno = 0;

            int nHashMethodIn = intMetodoHash;
            int nLenDataToSignIn = strDataEntrada.Length;
            string szDataToSignIn = strDataEntrada;
            string szInstitutionCode = strCodigoInstitucion;
            int nKeyNumber = intNumeroClave;
            string szFechayyyymmddhhmmss = strFechayyyymmddhhmmss;

            int nLenDataSignatureOut = 0;
            StringBuilder szDataSignatureOut = new StringBuilder(2048);

            codigoRetorno = cHeader.apissechsm_generate_rsa_signature_th(nHashMethodIn,
                                                                      nLenDataToSignIn,
                                                                      szDataToSignIn,
                                                                      szInstitutionCode,
                                                                      nKeyNumber,
                                                                      szFechayyyymmddhhmmss,
                                                                      out nLenDataSignatureOut,
                                                                      szDataSignatureOut);

            strDataSalida = szDataSignatureOut.ToString();
            strDataSalida = strDataSalida.Substring(0, nLenDataSignatureOut);

            return codigoRetorno;
        }

    }
}

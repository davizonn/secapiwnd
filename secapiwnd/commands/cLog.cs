using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.Diagnostics;
using System.Configuration;
using System.IO;

namespace secapiwnd.commands
{
    class Log
    {
        public const char DEB = 'D';
        public const char FAI = 'B';
        public const char ERR = 'E';
        public const char WAR = 'W';
        public const char INF = 'I';

        public Log()
        {
        }

        ~Log()
        {
        }

        private static string ObtenerRutaLog()
        {
            if (Util.strWorkDir.Length == 0)
                Util.strWorkDir = @"C:\Temp";

            if (!Util.strWorkDir.EndsWith(@"\"))
                Util.strWorkDir += @"\";

            return Util.strWorkDir;
        }

        public static void Insertar(char tipoLog, string message)
        {
            DateTime datNow = DateTime.Now;
            string archLog = String.Format("APINet{0:yyyyMMdd}.log", datNow);
            StreamWriter sw;

            try
            {
                sw = File.AppendText(ObtenerRutaLog() + archLog);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: Sw [" + ex.Message + "]");
                return;
            }

            string nomArch = "";
            string nomMetodo = "";
            int numLinea = 0;
            string nomProceso = "";
            int idProceso = 0;

            StackTrace st = new StackTrace(true);

            //if (st.FrameCount > 2)
            if (st.FrameCount >= 2)
            {
                int i = 1;
                StackFrame sf = st.GetFrame(i);
                nomArch = Path.GetFileName(sf.GetFileName());
                numLinea = sf.GetFileLineNumber();
                nomMetodo = sf.GetMethod().Name;
            }
            else
            {
                nomArch = "ArchivoGeneral";
                numLinea = st.FrameCount;
                nomMetodo = "MetodoGeneral";
            }

            nomProceso = Process.GetCurrentProcess().ProcessName;
            idProceso = Process.GetCurrentProcess().Id;

            string logLine = String.Format("{0:ddd dd MMM yyyy HH:mm:ss.fff} {1,15} {2,9} {3,15} {4,5} [{5}] ===>{6}: {7}", datNow, nomProceso, idProceso, nomArch, numLinea, tipoLog, nomMetodo, message);

            try
            {
                sw.WriteLine(logLine);
                /*sw.WriteLine("CD: " + Environment.CurrentDirectory);
                sw.WriteLine("SD: " + Environment.SystemDirectory);*/
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: " + ex.Message);
            }
            finally
            {
                sw.Close();
            }
        }
    }
}


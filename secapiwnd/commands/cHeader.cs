using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;


namespace secapiwnd.commands
{
    public static class cHeader
    {
        public enum LoadLibraryFlags : uint
        {
            DONT_RESOLVE_DLL_REFERENCES = 0x00000001,
            LOAD_IGNORE_CODE_AUTHZ_LEVEL = 0x00000010,
            LOAD_LIBRARY_AS_DATAFILE = 0x00000002,
            LOAD_LIBRARY_AS_DATAFILE_EXCLUSIVE = 0x00000040,
            LOAD_LIBRARY_AS_IMAGE_RESOURCE = 0x00000020,
            LOAD_WITH_ALTERED_SEARCH_PATH = 0x00000008
        };

        public static IntPtr hModule;
        public static int numError;
        public static string nomDLL;

        [DllImport("kernel32", SetLastError = true)]
        public static extern IntPtr LoadLibrary(string lpFileName);

        [DllImport("kernel32.dll")]
        public static extern IntPtr LoadLibraryEx(string lpFileName, IntPtr hFile, LoadLibraryFlags dwFlags);

        [DllImport("kernel32", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
        public static extern IntPtr GetProcAddress(IntPtr hModule, string procName);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool FreeLibrary(IntPtr hModule);

        [DllImport("kernel32.dll")]
        private static extern int FormatMessage(int dwFlags,
            int lpSource, int dwMessageId, int dwLanguageId,
            ref String lpBuffer, int nSize, int Arguments);

        public static string GetErrorMessage(int errorCode)
        {
            Log.Insertar(Log.DEB, "GetErrorMessage INI");
            int FORMAT_MESSAGE_ALLOCATE_BUFFER = 0x00000100;
            int FORMAT_MESSAGE_IGNORE_INSERTS = 0x00000200;
            int FORMAT_MESSAGE_FROM_SYSTEM = 0x00001000;

            int messageSize = 255;
            string lpMsgBuf = "";
            int dwFlags = FORMAT_MESSAGE_ALLOCATE_BUFFER
               | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS;

            int retVal = FormatMessage(dwFlags, 0, errorCode, 0,
                                       ref lpMsgBuf, messageSize, 0);
            Log.Insertar(Log.DEB, "GetErrorMessage FIN");
            if (0 == retVal)
            {
                return null;
            }
            else
            {
                return lpMsgBuf;
            }
        }
        /* CRV 20110228 FIN */

        /// <summary>
        /// Estructura con la data de respuesta
        /// </summary>
        public struct DataRespuesta
        {
            public string strDataSalida;
            public string strMensajeSalidaCifrado;
            public string strClaveDESSalidaCifrada;
            public string strMensajeSalidaDescifrado;
            public string strDescripcionError;
        }

        /* CRV 20110228 INI */
        //Delegate
        private delegate int DelegateGenerateRSAKey(
            int nPrivKeyLength,
            int nExpPublicLength,
            uint nExpPublic,
            out uint nPublicKeyLength,
            [MarshalAs(UnmanagedType.LPStr)] StringBuilder szPublicKey,
            out uint nPrivateKeyLength,
            [MarshalAs(UnmanagedType.LPStr)] StringBuilder szPrivateKey);

        //Function
        public static int apissechsm_generate_rsa_key(
            int nPrivKeyLength,
            int nExpPublicLength,
            uint nExpPublic,
            out uint nPublicKeyLength,
            StringBuilder szPublicKey,
            out uint nPrivateKeyLength,
            StringBuilder szPrivateKey)
        {
            Log.Insertar(Log.DEB, "apissechsm_generate_rsa_key INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr genRSAProcAddr = GetProcAddress(hModule, "apissechsm_generate_rsa_key");
            Log.Insertar(Log.DEB, "GetProcAddress. genRSAProcAddr [" + genRSAProcAddr.ToString() + "]");
            DelegateGenerateRSAKey genRSADelegate = (DelegateGenerateRSAKey)Marshal.GetDelegateForFunctionPointer(genRSAProcAddr, typeof(DelegateGenerateRSAKey));
            rc = genRSADelegate(nPrivKeyLength, nExpPublicLength, nExpPublic, out nPublicKeyLength, szPublicKey, out nPrivateKeyLength, szPrivateKey);
            Log.Insertar(Log.DEB, "apissechsm_generate_rsa_key FIN");
            return rc;
        }
        /* CRV 20110228 FIN */

        /* CRV 20110301 INI */
        //Delegate
        private delegate int DelegateGenerateRandomKey3DES(
            int nKeyType,
            int nLenType,
            out int nKeyLengthOut,
            StringBuilder szDESKeyOut,
            StringBuilder szCheckValueDESKeyOut);

        //Function
        public static int apissechsm_generate_random_key_3des(
            int nKeyType,
            int nLenType,
            out int nKeyLengthOut,
            StringBuilder szDESKeyOut,
            StringBuilder szCheckValueDESKeyOut)
        {
            Log.Insertar(Log.DEB, "apissechsm_generate_random_key_3des INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr genRandomKeyProcAddr = GetProcAddress(hModule, "apissechsm_generate_random_key_3des");
            Log.Insertar(Log.DEB, "GetProcAddress. genRandomKeyProcAddr [" + genRandomKeyProcAddr.ToString() + "]");
            DelegateGenerateRandomKey3DES genRandomKeyDelegate = (DelegateGenerateRandomKey3DES)Marshal.GetDelegateForFunctionPointer(genRandomKeyProcAddr, typeof(DelegateGenerateRandomKey3DES));
            rc = genRandomKeyDelegate(nKeyType, nLenType, out nKeyLengthOut, szDESKeyOut, szCheckValueDESKeyOut);
            Log.Insertar(Log.DEB, "apissechsm_generate_random_key_3des FIN");
            return rc;
        }


        //Delegate
        private delegate int DelegateGenerateRandomKey3DES_2(
            int nKeyType,
            int nLenType,
            out int nKeyLengthOut,
            StringBuilder szDESKeyOut,
            [MarshalAs(UnmanagedType.LPArray)] byte[] szCheckValueDESKeyOut);

        //Function
        public static int apissechsm_generate_random_key_3des(
            int nKeyType,
            int nLenType,
            out int nKeyLengthOut,
            StringBuilder szDESKeyOut,
            [MarshalAs(UnmanagedType.LPArray)] byte[] szCheckValueDESKeyOut)
        {
            Log.Insertar(Log.DEB, "apissechsm_generate_random_key_3des INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr genRandomKeyProcAddr = GetProcAddress(hModule, "apissechsm_generate_random_key_3des");
            Log.Insertar(Log.DEB, "GetProcAddress. genRandomKeyProcAddr [" + genRandomKeyProcAddr.ToString() + "]");
            DelegateGenerateRandomKey3DES_2 genRandomKeyDelegate = (DelegateGenerateRandomKey3DES_2)Marshal.GetDelegateForFunctionPointer(genRandomKeyProcAddr, typeof(DelegateGenerateRandomKey3DES_2));
            rc = genRandomKeyDelegate(nKeyType, nLenType, out nKeyLengthOut, szDESKeyOut, szCheckValueDESKeyOut);
            Log.Insertar(Log.DEB, "apissechsm_generate_random_key_3des FIN");
            return rc;
        }
        /* CRV 20110301 FIN */

        /* CRV 20110228 INI */
        //Delegate
        private delegate int DelegateEncryptMessage3DES(
            string szDESKeyCode,
            int nMessageLength,
            string szMessage,
            out int nEncryptedMessageLengthOut,
            StringBuilder szEncryptedMessageOut);

        //Function
        public static int apissechsm_encrypt_message_3des(
            string szDESKeyCode,
            int nMessageLength,
            string szMessage,
            out int nEncryptedMessageLengthOut,
            StringBuilder szEncryptedMessageOut)
        {
            Log.Insertar(Log.DEB, "apissechsm_encrypt_message_3des INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr crpMsgProcAddr = GetProcAddress(hModule, "apissechsm_encrypt_message_3des");
            Log.Insertar(Log.DEB, "GetProcAddress. crpMsgProcAddr [" + crpMsgProcAddr.ToString() + "]");
            DelegateEncryptMessage3DES crpMsgDelegate = (DelegateEncryptMessage3DES)Marshal.GetDelegateForFunctionPointer(crpMsgProcAddr, typeof(DelegateEncryptMessage3DES));
            rc = crpMsgDelegate(szDESKeyCode, nMessageLength, szMessage, out nEncryptedMessageLengthOut, szEncryptedMessageOut);
            Log.Insertar(Log.DEB, "apissechsm_encrypt_message_3des FIN");
            return rc;
        }
        /* CRV 20110228 FIN */

        /* CRV 20110228 INI */
        //Delegate
        private delegate int DelegateDecryptMessage3DES(
            string szDESKeyCode,
            int nEncryptedMessageLength,
            string szEncryptedMessage,
            out int nMessageLengthOut,
            StringBuilder szMessageOut);

        //Function
        public static int apissechsm_dencrypt_message_3des(
            string szDESKeyCode,
            int nEncryptedMessageLength,
            string szEncryptedMessage,
            out int nMessageLengthOut,
            StringBuilder szMessageOut)
        {
            Log.Insertar(Log.DEB, "apissechsm_dencrypt_message_3des INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr dcrpMsgProcAddr = GetProcAddress(hModule, "apissechsm_dencrypt_message_3des");
            Log.Insertar(Log.DEB, "GetProcAddress. dcrpMsgProcAddr [" + dcrpMsgProcAddr.ToString() + "]");
            DelegateDecryptMessage3DES dcrpMsgDelegate = (DelegateDecryptMessage3DES)Marshal.GetDelegateForFunctionPointer(dcrpMsgProcAddr, typeof(DelegateDecryptMessage3DES));
            rc = dcrpMsgDelegate(szDESKeyCode, nEncryptedMessageLength, szEncryptedMessage, out nMessageLengthOut, szMessageOut);
            Log.Insertar(Log.DEB, "apissechsm_dencrypt_message_3des FIN");
            return rc;
        }
        /* CRV 20110228 FIN */

        /* CRV 20110228 INI */
        //Delegate
        private delegate int DelegateLoadRSAPrivateKey(
            StringBuilder szInstitutionCode,
            int nKeyNumber,
            int nKeyIndexHSM);

        //Function
        public static int apissechsm_load_rsa_priv_key(
            StringBuilder szInstitutionCode,
            int nKeyNumber,
            int nKeyIndexHSM)
        {
            Log.Insertar(Log.DEB, "apissechsm_load_rsa_priv_key INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr ldPKIProcAddr = GetProcAddress(hModule, "apissechsm_load_rsa_priv_key");
            Log.Insertar(Log.DEB, "GetProcAddress. ldPKIProcAddr [" + ldPKIProcAddr.ToString() + "]");
            DelegateLoadRSAPrivateKey ldPKIDelegate = (DelegateLoadRSAPrivateKey)Marshal.GetDelegateForFunctionPointer(ldPKIProcAddr, typeof(DelegateLoadRSAPrivateKey));
            rc = ldPKIDelegate(szInstitutionCode, nKeyNumber, nKeyIndexHSM);
            Log.Insertar(Log.DEB, "apissechsm_load_rsa_priv_key FIN");
            return rc;
        }
        /* CRV 20110228 FIN */

        /* CRV 20110228 INI */
        //Delegate
        private delegate int DelegateGenRSASignature(
            int nHashMethodIn,
            int nLenDataToSignIn,
            string szDataToSignIn,
            string szInstitutionCode,
            int nKeyNumber,
            out int nLenDataSignatureOut,
            StringBuilder szDataSignatureOut);

        //Function
        public static int apissechsm_generate_rsa_signature(
            int nHashMethodIn,
            int nLenDataToSignIn,
            string szDataToSignIn,
            string szInstitutionCode,
            int nKeyNumber,
            out int nLenDataSignatureOut,
            StringBuilder szDataSignatureOut)
        {
            Log.Insertar(Log.DEB, "apissechsm_generate_rsa_signature INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr genRSASignProcAddr = GetProcAddress(hModule, "apissechsm_generate_rsa_signature");
            Log.Insertar(Log.DEB, "GetProcAddress. genRSASignProcAddr [" + genRSASignProcAddr.ToString() + "]");
            DelegateGenRSASignature genRSASignDelegate = (DelegateGenRSASignature)Marshal.GetDelegateForFunctionPointer(genRSASignProcAddr, typeof(DelegateGenRSASignature));
            rc = genRSASignDelegate(nHashMethodIn, nLenDataToSignIn, szDataToSignIn, szInstitutionCode, nKeyNumber, out nLenDataSignatureOut, szDataSignatureOut);
            Log.Insertar(Log.DEB, "apissechsm_generate_rsa_signature FIN");
            return rc;
        }
        /* CRV 20110228 FIN */

        /* CRV 20110228 INI */
        //Delegate
        private delegate int DelegateValRSASignature(
            int nHashMethodIn,
            int nLenDataSignedIn,
            string szDataSignedIn,
            int nLenDataSignatureIn,
            string szDataSignatureIn,
            string szSourceInstitutionCodeIn,
            int nPublicKeyIndexIn);

        //Function
        public static int apissechsm_validate_rsa_signature(
            int nHashMethodIn,
            int nLenDataSignedIn,
            string szDataSignedIn,
            int nLenDataSignatureIn,
            string szDataSignatureIn,
            string szSourceInstitutionCodeIn,
            int nPublicKeyIndexIn)
        {
            Log.Insertar(Log.DEB, "apissechsm_validate_rsa_signature INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr valRSASignProcAddr = GetProcAddress(hModule, "apissechsm_validate_rsa_signature");
            Log.Insertar(Log.DEB, "GetProcAddress. valRSASignProcAddr [" + valRSASignProcAddr.ToString() + "]");
            DelegateValRSASignature valRSASignDelegate = (DelegateValRSASignature)Marshal.GetDelegateForFunctionPointer(valRSASignProcAddr, typeof(DelegateValRSASignature));
            rc = valRSASignDelegate(nHashMethodIn, nLenDataSignedIn, szDataSignedIn, nLenDataSignatureIn, szDataSignatureIn, szSourceInstitutionCodeIn, nPublicKeyIndexIn);
            Log.Insertar(Log.DEB, "apissechsm_validate_rsa_signature FIN");
            return rc;
        }
        /* CRV 20110228 FIN */

        /* CRV 20110228 INI */
        //Delegate
        private delegate int DelegateGenRSAAuthMsg(
            string szSourceInstitutionCode,
            string szSourceDESKeyCode,
            string szInstitutionCode,
            int nPublicKeyIndex,
            out int nLenProtectedMessageOut,
            StringBuilder szProtectedMessageOut,
            out int nLenProtectedDESKeyOut,
            StringBuilder szProtectedDESKeyOut);

        //Function
        public static int apissechsm_generate_rsa_auth_message(
            string szSourceInstitutionCode,
            string szSourceDESKeyCode,
            string szInstitutionCode,
            int nPublicKeyIndex,
            out int nLenProtectedMessageOut,
            StringBuilder szProtectedMessageOut,
            out int nLenProtectedDESKeyOut,
            StringBuilder szProtectedDESKeyOut)
        {
            Log.Insertar(Log.DEB, "apissechsm_generate_rsa_auth_message INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr genRSAMsgProcAddr = GetProcAddress(hModule, "apissechsm_generate_rsa_auth_message");
            Log.Insertar(Log.DEB, "GetProcAddress. genRSAMsgProcAddr [" + genRSAMsgProcAddr.ToString() + "]");
            DelegateGenRSAAuthMsg genRSAMsgDelegate = (DelegateGenRSAAuthMsg)Marshal.GetDelegateForFunctionPointer(genRSAMsgProcAddr, typeof(DelegateGenRSAAuthMsg));
            rc = genRSAMsgDelegate(szSourceInstitutionCode, szSourceDESKeyCode, szInstitutionCode, nPublicKeyIndex, out nLenProtectedMessageOut, szProtectedMessageOut, out nLenProtectedDESKeyOut, szProtectedDESKeyOut);
            Log.Insertar(Log.DEB, "apissechsm_generate_rsa_auth_message FIN");
            return rc;
        }
        /* CRV 20110228 FIN */

        /* CRV 20110228 INI */
        //Delegate
        private delegate int DelegateValRSAAuthMsg(
            int nLenProtectedMessage,
            string szProtectedMessage,
            int nLenProtectedDESKey,
            string szProtectedDESKey,
            string szDESKeyCode,
            string szInstitutionCode,
            int nKeyNumber,
            string szSourceInstitutionCode);

        //Function
        public static int apissechsm_validate_rsa_auth_message(
            int nLenProtectedMessage,
            string szProtectedMessage,
            int nLenProtectedDESKey,
            string szProtectedDESKey,
            string szDESKeyCode,
            string szInstitutionCode,
            int nKeyNumber,
            string szSourceInstitutionCode)
        {
            Log.Insertar(Log.DEB, "apissechsm_validate_rsa_auth_message INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr valRSAMsgProcAddr = GetProcAddress(hModule, "apissechsm_validate_rsa_auth_message");
            Log.Insertar(Log.DEB, "GetProcAddress. valRSAMsgProcAddr [" + valRSAMsgProcAddr.ToString() + "]");
            DelegateValRSAAuthMsg valRSAMsgDelegate = (DelegateValRSAAuthMsg)Marshal.GetDelegateForFunctionPointer(valRSAMsgProcAddr, typeof(DelegateValRSAAuthMsg));
            rc = valRSAMsgDelegate(nLenProtectedMessage, szProtectedMessage, nLenProtectedDESKey, szProtectedDESKey, szDESKeyCode, szInstitutionCode, nKeyNumber, szSourceInstitutionCode);
            Log.Insertar(Log.DEB, "apissechsm_validate_rsa_auth_message FIN");
            return rc;
        }
        /* CRV 20110228 FIN */

        /* CRV 20110228 INI */
        //Delegate
        private delegate int DelegateEncrypt3DESRSAMsg(
            int nLenMessageIn,
            string szMessageIn,
            string szTargetInstitutionCodeIn,
            int nTargetPublicKeyIndexIn,
            out int nLenProtectedMessageOut,
            StringBuilder szProtectedMessageOut,
            out int nLenProtectedDESKeyOut,
            StringBuilder szProtectedDESKeyOut);

        //Function
        public static int apissechsm_encrypt_3des_rsa_message(
            int nLenMessageIn,
            string szMessageIn,
            string szTargetInstitutionCodeIn,
            int nTargetPublicKeyIndexIn,
            out int nLenProtectedMessageOut,
            StringBuilder szProtectedMessageOut,
            out int nLenProtectedDESKeyOut,
            StringBuilder szProtectedDESKeyOut)
        {
            Log.Insertar(Log.DEB, "apissechsm_encrypt_3des_rsa_message INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr crpMsgProcAddr = GetProcAddress(hModule, "apissechsm_encrypt_3des_rsa_message");
            Log.Insertar(Log.DEB, "GetProcAddress. crpMsgProcAddr [" + crpMsgProcAddr.ToString() + "]");
            DelegateEncrypt3DESRSAMsg crpMsgDelegate = (DelegateEncrypt3DESRSAMsg)Marshal.GetDelegateForFunctionPointer(crpMsgProcAddr, typeof(DelegateEncrypt3DESRSAMsg));
            rc = crpMsgDelegate(nLenMessageIn, szMessageIn, szTargetInstitutionCodeIn, nTargetPublicKeyIndexIn, out nLenProtectedMessageOut, szProtectedMessageOut, out nLenProtectedDESKeyOut, szProtectedDESKeyOut);
            Log.Insertar(Log.DEB, "apissechsm_encrypt_3des_rsa_message FIN");
            return rc;
        }
        /* CRV 20110228 FIN */

        /* CRV 20110228 INI */
        //Delegate
        private delegate int DelegateDecrypt3DESRSAMsg(
            int nLenProtectedMessageIn,
            string szProtectedMessageIn,
            int nLenProtectedDESKeyIn,
            string szProtectedDESKeyIn,
            string szInstitutionCode,
            int nKeyNumber,
            out uint nLenDecryptedMessageOut,
            StringBuilder szDecryptedMessageOut);

        //Function
        public static int apissechsm_decrypt_3des_rsa_message(
            int nLenProtectedMessageIn,
            string szProtectedMessageIn,
            int nLenProtectedDESKeyIn,
            string szProtectedDESKeyIn,
            string szInstitutionCode,
            int nKeyNumber,
            out uint nLenDecryptedMessageOut,
            StringBuilder szDecryptedMessageOut)
        {
            Log.Insertar(Log.DEB, "apissechsm_decrypt_3des_rsa_message INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr dcrpMsgProcAddr = GetProcAddress(hModule, "apissechsm_decrypt_3des_rsa_message");
            Log.Insertar(Log.DEB, "GetProcAddress. dcrpMsgProcAddr [" + dcrpMsgProcAddr.ToString() + "]");
            DelegateDecrypt3DESRSAMsg dcrpMsgDelegate = (DelegateDecrypt3DESRSAMsg)Marshal.GetDelegateForFunctionPointer(dcrpMsgProcAddr, typeof(DelegateDecrypt3DESRSAMsg));
            rc = dcrpMsgDelegate(nLenProtectedMessageIn, szProtectedMessageIn, nLenProtectedDESKeyIn, szProtectedDESKeyIn, szInstitutionCode, nKeyNumber, out nLenDecryptedMessageOut, szDecryptedMessageOut);
            Log.Insertar(Log.DEB, "apissechsm_decrypt_3des_rsa_message FIN");
            return rc;
        }
        /* CRV 20110228 FIN */

        ///////////////////////////////////////////////////////////////////////////////////////////
        // 
        // Lista de Funciones Historicas
        //
        ///////////////////////////////////////////////////////////////////////////////////////////

        /* CRV 20110228 INI */
        //Delegate
        private delegate int DelegateEncryptMsg3DESTH(
            string szDESKeyCode,
            int nMessageLength,
            string szMessage,
            string szFechayyyymmddhhmmss,
            out int nEncryptedMessageLengthOut,
            StringBuilder szEncryptedMessageOut);

        //Function
        public static int apissechsm_encrypt_message_3des_th(
            string szDESKeyCode,
            int nMessageLength,
            string szMessage,
            string szFechayyyymmddhhmmss,
            out int nEncryptedMessageLengthOut,
            StringBuilder szEncryptedMessageOut)
        {
            Log.Insertar(Log.DEB, "apissechsm_encrypt_message_3des_th INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr crpMsgTHProcAddr = GetProcAddress(hModule, "apissechsm_encrypt_message_3des_th");
            Log.Insertar(Log.DEB, "GetProcAddress. crpMsgTHProcAddr [" + crpMsgTHProcAddr.ToString() + "]");
            DelegateEncryptMsg3DESTH crpMsgTHDelegate = (DelegateEncryptMsg3DESTH)Marshal.GetDelegateForFunctionPointer(crpMsgTHProcAddr, typeof(DelegateEncryptMsg3DESTH));
            rc = crpMsgTHDelegate(szDESKeyCode, nMessageLength, szMessage, szFechayyyymmddhhmmss, out nEncryptedMessageLengthOut, szEncryptedMessageOut);
            Log.Insertar(Log.DEB, "apissechsm_encrypt_message_3des_th FIN");
            return rc;
        }
        /* CRV 20110228 FIN */

        /* CRV 20110228 INI */
        //Delegate
        private delegate int DelegateDecryptMsg3DESTH(
            string szDESKeyCode,
            int nEncryptedMessageLength,
            string szEncryptedMessage,
            string szFechayyyymmddhhmmss,
            out int nMessageLengthOut,
            StringBuilder szMessageOut);

        //Function
        public static int apissechsm_dencrypt_message_3des_th(
            string szDESKeyCode,
            int nEncryptedMessageLength,
            string szEncryptedMessage,
            string szFechayyyymmddhhmmss,
            out int nMessageLengthOut,
            StringBuilder szMessageOut)
        {
            Log.Insertar(Log.DEB, "apissechsm_dencrypt_message_3des_th INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr dcrpMsgTHProcAddr = GetProcAddress(hModule, "apissechsm_dencrypt_message_3des_th");
            Log.Insertar(Log.DEB, "GetProcAddress. dcrpMsgTHProcAddr [" + dcrpMsgTHProcAddr.ToString() + "]");
            DelegateDecryptMsg3DESTH dcrpMsgTHDelegate = (DelegateDecryptMsg3DESTH)Marshal.GetDelegateForFunctionPointer(dcrpMsgTHProcAddr, typeof(DelegateDecryptMsg3DESTH));
            rc = dcrpMsgTHDelegate(szDESKeyCode, nEncryptedMessageLength, szEncryptedMessage, szFechayyyymmddhhmmss, out nMessageLengthOut, szMessageOut);
            Log.Insertar(Log.DEB, "apissechsm_dencrypt_message_3des_th FIN");
            return rc;
        }
        /* CRV 20110228 FIN */

        /* CRV 20110228 INI */
        //Delegate
        private delegate int DelegateGenerateRSASignatureTH(
            int nHashMethodIn,
            int nLenDataToSignIn,
            string szDataToSignIn,
            string szInstitutionCode,
            int nKeyNumber,
            string szFechayyyymmddhhmmss,
            out int nLenDataSignatureOut,
            StringBuilder szDataSignatureOut);

        //Function
        public static int apissechsm_generate_rsa_signature_th(
            int nHashMethodIn,
            int nLenDataToSignIn,
            string szDataToSignIn,
            string szInstitutionCode,
            int nKeyNumber,
            string szFechayyyymmddhhmmss,
            out int nLenDataSignatureOut,
            StringBuilder szDataSignatureOut)
        {
            Log.Insertar(Log.DEB, "apissechsm_generate_rsa_signature_th INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr genRSASignTHProcAddr = GetProcAddress(hModule, "apissechsm_generate_rsa_signature_th");
            Log.Insertar(Log.DEB, "GetProcAddress. genRSASignTHProcAddr [" + genRSASignTHProcAddr.ToString() + "]");
            DelegateGenerateRSASignatureTH genRSASignTHDelegate = (DelegateGenerateRSASignatureTH)Marshal.GetDelegateForFunctionPointer(genRSASignTHProcAddr, typeof(DelegateGenerateRSASignatureTH));
            rc = genRSASignTHDelegate(nHashMethodIn, nLenDataToSignIn, szDataToSignIn, szInstitutionCode, nKeyNumber, szFechayyyymmddhhmmss, out nLenDataSignatureOut, szDataSignatureOut);
            Log.Insertar(Log.DEB, "apissechsm_generate_rsa_signature_th FIN");
            return rc;
        }
        /* CRV 20110228 FIN */

        /* CRV 20110228 INI */
        //Delegate
        private delegate int DelegateValidateRSASignatureTH(
            int nHashMethodIn,
            int nLenDataSignedIn,
            string szDataSignedIn,
            int nLenDataSignatureIn,
            string szDataSignatureIn,
            string szSourceInstitutionCodeIn,
            int nPublicKeyIndexIn,
            string szFechayyyymmddhhmmss);

        //Function
        public static int apissechsm_validate_rsa_signature_th(
            int nHashMethodIn,
            int nLenDataSignedIn,
            string szDataSignedIn,
            int nLenDataSignatureIn,
            string szDataSignatureIn,
            string szSourceInstitutionCodeIn,
            int nPublicKeyIndexIn,
            string szFechayyyymmddhhmmss)
        {
            Log.Insertar(Log.DEB, "apissechsm_validate_rsa_signature_th INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr valRSASignTHProcAddr = GetProcAddress(hModule, "apissechsm_validate_rsa_signature_th");
            Log.Insertar(Log.DEB, "GetProcAddress. valRSASignTHProcAddr [" + valRSASignTHProcAddr.ToString() + "]");
            DelegateValidateRSASignatureTH valRSASignTHDelegate = (DelegateValidateRSASignatureTH)Marshal.GetDelegateForFunctionPointer(valRSASignTHProcAddr, typeof(DelegateValidateRSASignatureTH));
            rc = valRSASignTHDelegate(nHashMethodIn, nLenDataSignedIn, szDataSignedIn, nLenDataSignatureIn, szDataSignatureIn, szSourceInstitutionCodeIn, nPublicKeyIndexIn, szFechayyyymmddhhmmss);
            Log.Insertar(Log.DEB, "apissechsm_validate_rsa_signature_th FIN");
            return rc;
        }
        /* CRV 20110228 FIN */

        /* CRV 20110228 INI */
        //Delegate
        private delegate int DelegateGenerateRSAAuthMsgTH(
            string szSourceInstitutionCode,
            string szSourceDESKeyCode,
            string szInstitutionCode,
            int nPublicKeyIndex,
            string szFechayyyymmddhhmmss,
            out int nLenProtectedMessageOut,
            StringBuilder szProtectedMessageOut,
            out int nLenProtectedDESKeyOut,
            StringBuilder szProtectedDESKeyOut);

        //Function
        public static int apissechsm_generate_rsa_auth_message_th(
            string szSourceInstitutionCode,
            string szSourceDESKeyCode,
            string szInstitutionCode,
            int nPublicKeyIndex,
            string szFechayyyymmddhhmmss,
            out int nLenProtectedMessageOut,
            StringBuilder szProtectedMessageOut,
            out int nLenProtectedDESKeyOut,
            StringBuilder szProtectedDESKeyOut)
        {
            Log.Insertar(Log.DEB, "apissechsm_generate_rsa_auth_message_th INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr genMsgTHProcAddr = GetProcAddress(hModule, "apissechsm_generate_rsa_auth_message_th");
            Log.Insertar(Log.DEB, "GetProcAddress. genMsgTHProcAddr [" + genMsgTHProcAddr.ToString() + "]");
            DelegateGenerateRSAAuthMsgTH genMsgTHDelegate = (DelegateGenerateRSAAuthMsgTH)Marshal.GetDelegateForFunctionPointer(genMsgTHProcAddr, typeof(DelegateGenerateRSAAuthMsgTH));
            rc = genMsgTHDelegate(szSourceInstitutionCode, szSourceDESKeyCode, szInstitutionCode, nPublicKeyIndex, szFechayyyymmddhhmmss, out nLenProtectedMessageOut, szProtectedMessageOut, out nLenProtectedDESKeyOut, szProtectedDESKeyOut);
            Log.Insertar(Log.DEB, "apissechsm_generate_rsa_auth_message_th FIN");
            return rc;
        }
        /* CRV 20110228 FIN */

        /* CRV 20110228 INI */
        //Delegate
        private delegate int DelegateValidateRSAAuthMsgTH(
            int nLenProtectedMessage,
            string szProtectedMessage,
            int nLenProtectedDESKey,
            string szProtectedDESKey,
            string szDESKeyCode,
            string szInstitutionCode,
            int nKeyNumber,
            string szSourceInstitutionCode,
            string szFechayyyymmddhhmmss);

        //Function
        public static int apissechsm_validate_rsa_auth_message_th(
            int nLenProtectedMessage,
            string szProtectedMessage,
            int nLenProtectedDESKey,
            string szProtectedDESKey,
            string szDESKeyCode,
            string szInstitutionCode,
            int nKeyNumber,
            string szSourceInstitutionCode,
            string szFechayyyymmddhhmmss)
        {
            Log.Insertar(Log.DEB, "apissechsm_validate_rsa_auth_message_th INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr valMsgTHProcAddr = GetProcAddress(hModule, "apissechsm_validate_rsa_auth_message_th");
            Log.Insertar(Log.DEB, "GetProcAddress. valMsgTHProcAddr [" + valMsgTHProcAddr.ToString() + "]");
            DelegateValidateRSAAuthMsgTH valMsgTHDelegate = (DelegateValidateRSAAuthMsgTH)Marshal.GetDelegateForFunctionPointer(valMsgTHProcAddr, typeof(DelegateValidateRSAAuthMsgTH));
            rc = valMsgTHDelegate(nLenProtectedMessage, szProtectedMessage, nLenProtectedDESKey, szProtectedDESKey, szDESKeyCode, szInstitutionCode, nKeyNumber, szSourceInstitutionCode, szFechayyyymmddhhmmss);
            Log.Insertar(Log.DEB, "apissechsm_validate_rsa_auth_message_th FIN");
            return rc;
        }
        /* CRV 20110228 FIN */

        /* CRV 20110228 INI */
        //Delegate
        private delegate int DelegateEncrypt3DESRSAMsgTH(
            int nLenMessageIn,
            string szMessageIn,
            string szTargetInstitutionCodeIn,
            int nTargetPublicKeyIndexIn,
            string szFechayyyymmddhhmmss,
            out int nLenProtectedMessageOut,
            StringBuilder szProtectedMessageOut,
            out int nLenProtectedDESKeyOut,
            StringBuilder szProtectedDESKeyOut);

        //Function
        public static int apissechsm_encrypt_3des_rsa_message_th(
            int nLenMessageIn,
            string szMessageIn,
            string szTargetInstitutionCodeIn,
            int nTargetPublicKeyIndexIn,
            string szFechayyyymmddhhmmss,
            out int nLenProtectedMessageOut,
            StringBuilder szProtectedMessageOut,
            out int nLenProtectedDESKeyOut,
            StringBuilder szProtectedDESKeyOut)
        {
            Log.Insertar(Log.DEB, "apissechsm_encrypt_3des_rsa_message_th INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr crpMsgTHProcAddr = GetProcAddress(hModule, "apissechsm_encrypt_3des_rsa_message_th");
            Log.Insertar(Log.DEB, "GetProcAddress. crpMsgTHProcAddr [" + crpMsgTHProcAddr.ToString() + "]");
            DelegateEncrypt3DESRSAMsgTH crpMsgTHDelegate = (DelegateEncrypt3DESRSAMsgTH)Marshal.GetDelegateForFunctionPointer(crpMsgTHProcAddr, typeof(DelegateEncrypt3DESRSAMsgTH));
            rc = crpMsgTHDelegate(nLenMessageIn, szMessageIn, szTargetInstitutionCodeIn, nTargetPublicKeyIndexIn, szFechayyyymmddhhmmss, out nLenProtectedMessageOut, szProtectedMessageOut, out nLenProtectedDESKeyOut, szProtectedDESKeyOut);
            Log.Insertar(Log.DEB, "apissechsm_encrypt_3des_rsa_message_th FIN");
            return rc;
        }
        /* CRV 20110228 FIN */

        /* CRV 20110228 INI */
        //Delegate
        private delegate int DelegateDecrypt3DESRSAMsgTH(
            int nLenProtectedMessageIn,
            string szProtectedMessageIn,
            int nLenProtectedDESKeyIn,
            string szProtectedDESKeyIn,
            string szInstitutionCode,
            int nKeyNumber,
            string szFechayyyymmddhhmmss,
            out uint nLenDecryptedMessageOut,
            StringBuilder szDecryptedMessageOut);

        //Function
        public static int apissechsm_decrypt_3des_rsa_message_th(
            int nLenProtectedMessageIn,
            string szProtectedMessageIn,
            int nLenProtectedDESKeyIn,
            string szProtectedDESKeyIn,
            string szInstitutionCode,
            int nKeyNumber,
            string szFechayyyymmddhhmmss,
            out uint nLenDecryptedMessageOut,
            StringBuilder szDecryptedMessageOut)
        {
            Log.Insertar(Log.DEB, "apissechsm_decrypt_3des_rsa_message_th INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr dcrpMsgTHProcAddr = GetProcAddress(hModule, "apissechsm_decrypt_3des_rsa_message_th");
            Log.Insertar(Log.DEB, "GetProcAddress. dcrpMsgTHProcAddr [" + dcrpMsgTHProcAddr.ToString() + "]");
            DelegateDecrypt3DESRSAMsgTH dcrpMsgTHDelegate = (DelegateDecrypt3DESRSAMsgTH)Marshal.GetDelegateForFunctionPointer(dcrpMsgTHProcAddr, typeof(DelegateDecrypt3DESRSAMsgTH));
            rc = dcrpMsgTHDelegate(nLenProtectedMessageIn, szProtectedMessageIn, nLenProtectedDESKeyIn, szProtectedDESKeyIn, szInstitutionCode, nKeyNumber, szFechayyyymmddhhmmss, out nLenDecryptedMessageOut, szDecryptedMessageOut);
            Log.Insertar(Log.DEB, "apissechsm_decrypt_3des_rsa_message_th FIN");
            return rc;
        }
        /* CRV 20110228 FIN */

        /* CRV 20110228 INI */
        //Delegate
        private delegate int DelegateDBConnect(
            string szServer,
            string szDatabase,
            string szDbUser,
            string szDbPassword);

        //Function
        public static int apissechsm_db_connect(
            string szServer,
            string szDatabase,
            string szDbUser,
            string szDbPassword)
        {
            Log.Insertar(Log.DEB, "apissechsm_db_connect INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr dbConnectProcAddr = GetProcAddress(hModule, "apissechsm_db_connect");
            Log.Insertar(Log.DEB, "GetProcAddress. dbConnectProcAddr [" + dbConnectProcAddr.ToString() + "]");
            DelegateDBConnect dbConnectDelegate = (DelegateDBConnect)Marshal.GetDelegateForFunctionPointer(dbConnectProcAddr, typeof(DelegateDBConnect));
            rc = dbConnectDelegate(szServer, szDatabase, szDbUser, szDbPassword);
            Log.Insertar(Log.DEB, "apissechsm_db_connect FIN");
            return rc;
        }
        /* CRV 20110228 FIN */

        /* CRV 20110228 INI */
        //Delegate
        private delegate int DelegateGetErrorMessage(
            int nCodeError,
            out uint nSourceOut,
            out uint nLevelOut,
            StringBuilder szDescriptionOut);

        //Function
        public static int apissechsm_get_error_message(
            int nCodeError,
            out uint nSourceOut,
            out uint nLevelOut,
            StringBuilder szDescriptionOut)
        {
            Log.Insertar(Log.DEB, "apissechsm_get_error_message INI");
            int rc;

            if (hModule.Equals(IntPtr.Zero))
            {
                Log.Insertar(Log.ERR, "Se recarga librer�a por error de puntero nulo");
                Util.CargarLibreria();
            }
            IntPtr getErrorMsgProcAddr = GetProcAddress(hModule, "apissechsm_get_error_message");
            Log.Insertar(Log.DEB, "GetProcAddress. getErrorMsgProcAddr [" + getErrorMsgProcAddr.ToString() + "]");
            DelegateGetErrorMessage getErrorMsgDelegate = (DelegateGetErrorMessage)Marshal.GetDelegateForFunctionPointer(getErrorMsgProcAddr, typeof(DelegateGetErrorMessage));
            rc = getErrorMsgDelegate(nCodeError, out nSourceOut, out nLevelOut, szDescriptionOut);
            Log.Insertar(Log.DEB, "apissechsm_get_error_message FIN");
            return rc;
        }
        /* CRV 20110228 FIN */

    }
}
